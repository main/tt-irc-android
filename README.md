Tiny Tiny IRC client for Android
================================

Main project wiki: https://git.tt-rss.org/git/tt-irc/wiki

http://tt-rss.org/tt-irc-android

Licensed under GNU GPL version 3.
