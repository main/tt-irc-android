package org.fox.ttirc;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viewpagerindicator.TitlePageIndicator;

import org.fox.ttirc.backend.UpdateService;
import org.fox.ttirc.backend.UpdateServiceApi;
import org.fox.ttirc.backend.UpdateServiceListener;
import org.fox.ttirc.types.Channel;
import org.fox.ttirc.types.Connection;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class ChannelPager extends Fragment implements ChannelView {

	private final String TAG = "ChannelPager";
	private final String LISTENER_IDENT = "ChannelPager";
	
	private PagerAdapter m_adapter;
	private SharedPreferences m_prefs;
	
	private boolean m_initialUpdate = true;
	private int m_currentPage;
	private ViewPager m_pager;
	
	private int m_connectionId;
	private String m_channel;
	
	private List<ConnectionListEntry> m_connections = new ArrayList<ConnectionListEntry>();
	private TitlePageIndicator m_indicator;
	
	private UpdateServiceApi m_api;
	
	private MainActivity m_activity;
	
	public UpdateServiceListener m_updateListener = new UpdateServiceListener() {
		@Override
		public void handleUpdate() {
			//Log.i(TAG, "Received update request from the service");

			if (m_api == null || m_activity == null)
				return;

			final List<Connection> connections = m_api.getConnections();

			if (connections == null || getView() == null)
				return;

			m_connections.clear();

			for (Connection conn : connections) {
				try {
					if (m_api == null)
						return;
					
					Bundle counters = m_api.getCounters(conn.id, "---");

					m_connections.add(new ConnectionListEntry(conn,
							counters.getInt("unread")));

					SortedMap<String, Channel> channels;
					
					try {
						channels = new TreeMap<String, Channel>(m_api.getChannels(conn.id));
					} catch (NullPointerException e) {
						channels = null;
					}

					if (channels != null) {
						for (String channel : channels.keySet()) {
							Channel ichan = channels.get(channel);
							
							counters = m_api.getCounters(conn.id,
									channel);

							boolean isOffline = false;
							
							// 0 - C, 1 - P
							if (ichan.chan_type == 1) {
								isOffline = !conn.userhosts.containsKey(channel);
							}

							m_connections.add(new ConnectionListEntry(
									conn.id, ichan, channel, counters
											.getInt("unread"), isOffline));
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			
			m_activity.runOnUiThread(new Runnable() {
				public void run() {
					m_adapter.notifyDataSetChanged();

					if (m_initialUpdate) {
						m_pager.setCurrentItem(getItemPosition(m_connectionId, m_channel), false);
						m_initialUpdate = false;
					} else {
						m_pager.setCurrentItem(m_currentPage, false);
					}

					m_adapter.notifyDataSetChanged();
					m_indicator.invalidate();

				}
			});

		}
	};

	
	public ServiceConnection m_serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "Service connection established");
			m_api = (UpdateServiceApi)service;

			m_api.addListener(LISTENER_IDENT, m_updateListener);
			m_updateListener.handleUpdate();
		
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "Service connection closed");
			m_api = null;
		}
	};

	
	private class PagerAdapter extends FragmentStatePagerAdapter {
		
		public PagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
	    public String getPageTitle(int position) {
			ConnectionListEntry cl = m_connections.get(position);
			
	        return cl.m_title + (cl.m_numUnread > 0 ? " (" + cl.m_numUnread + ")" : "");
	    }
		
		@Override
		public Fragment getItem(int position) {
			try {
				ConnectionListEntry cl = m_connections.get(position);
				
				if (cl != null) {
					ChannelFragment cf = new ChannelFragment();
					cf.initialize(cl.m_connectionId, cl.m_nativeTitle);
					
					//Log.d(TAG, "creating fragment: " + cl.m_title);
					
					return cf;
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			
			return null;
		}

		@Override
		public int getCount() {
			return m_connections.size();
		}
		
	}
	
	public ChannelPager() {
		//
	}
		
	public void initialize(int connectionId, String channel) {
		m_connectionId = connectionId;
		m_channel = channel;
	}

	@Override
	public String getChannel() {
		return m_channel;
	}

	@Override
	public int getConnectionId() {
		return m_connectionId;
	}

	// FIXME
	@Override
	public int getMessagesCount() {
		return 0;
	}

    @Override
    public void appendText(String text) {
        // does nothing for now
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    	
		View view = inflater.inflate(R.layout.channel_pager, container, false);
	
		if (savedInstanceState != null) {
			m_channel = savedInstanceState.getString("channel");
			m_connectionId = savedInstanceState.getInt("connectionId");
			m_currentPage = savedInstanceState.getInt("currentPage");
		}
		
		m_adapter = new PagerAdapter(getActivity().getSupportFragmentManager());
		
		m_pager = view.findViewById(R.id.channel_pager);

		m_pager.setAdapter(m_adapter);
		m_pager.setCurrentItem(getItemPosition(m_connectionId, m_channel));
		
		m_indicator = view.findViewById(R.id.article_titles);
		m_indicator.setViewPager(m_pager);
		
		if (!m_activity.isUiNightMode()) {
				m_indicator.setTextColor(getResources().getColor(android.R.color.black));
				m_indicator.setSelectedColor(getResources().getColor(android.R.color.black));				
		}
		
		if (!m_activity.isSmallScreen() && !(m_activity.isPortrait() && m_prefs.getBoolean("hide_sidebar_in_portrait_mode", false))) {
			m_indicator.setVisibility(View.GONE);
		}
		
		m_indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageSelected(int position) {
				m_currentPage = position;
				
				ConnectionListEntry cl = m_connections.get(position);
				
				if (cl != null) {
					m_connectionId = cl.m_connectionId;
					m_channel = cl.m_nativeTitle;
				
					m_activity.onChannelSelected(m_connectionId, m_channel, false);
				}
			}
		}); 
	
		return view;
	}
		
	private int getItemPosition(int connectionId, String channel) {
		for (int i = 0; i < m_connections.size(); i++) {
			ConnectionListEntry cl = m_connections.get(i);
			
			if (cl.m_connectionId == connectionId && cl.m_nativeTitle.equals(channel)) {				
				return i;
			}
		}		
		
		return 0;
	}

	@Override
	public void onSaveInstanceState(Bundle out) {
		super.onSaveInstanceState(out);
		
		out.putString("channel", m_channel);
		out.putInt("connectionId", m_connectionId);
		out.putInt("currentPage", m_currentPage);
	}
	
	@Override
	public void onResume() {
		super.onResume();

		m_activity.bindService(new Intent(m_activity, 
	            UpdateService.class), m_serviceConnection, 0);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		m_prefs = PreferenceManager.getDefaultSharedPreferences(activity
				.getApplicationContext());
		
		m_activity = (MainActivity) activity;
	}

	/* public void setActiveArticle(Article article) {
		if (m_article != article) {
			m_article = article;

			int position = m_articles.indexOf(m_article);

			ViewPager pager = (ViewPager) getView().findViewById(R.id.article_pager);
		
			pager.setCurrentItem(position);
		}
	} */

	/* public void selectArticle(boolean next) {
		if (m_article != null) {
			int position = m_articles.indexOf(m_article);
			
			if (next) 
				position++;
			else
				position--;
			
			try {
				Article tmp = m_articles.get(position);
				
				if (tmp != null) {
					setActiveArticle(tmp);
				}
				
			} catch (IndexOutOfBoundsException e) {
				// do nothing
			}
		}		
	} */

	/* public void notifyUpdated() {
		m_adapter.notifyDataSetChanged();
	} */
	
	@Override
	public void onPause() {
		super.onPause();
		
		if (m_api != null) {
			m_api.removeListener(LISTENER_IDENT);
		}

		m_activity.unbindService(m_serviceConnection);
	}

	public void setActiveChannel(int connectionId, String channel) {
		int position = getItemPosition(connectionId, channel);
		
		m_pager.setCurrentItem(position, false);
	}
}
