package org.fox.ttirc.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.fox.ttirc.R;
import org.fox.ttirc.types.Constants;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpRequestTask extends AsyncTask<HashMap<String, String>, Integer, JsonElement> {
	private final String TAG = "HttpRequestTask";

	public enum ErrorType {
		NO_ERROR, NOT_CONFIGURED, NOT_AUTHORIZED, HTTP_ERROR, NEED_UPDATE, HTTP_FORBIDDEN, PARSE_ERROR,
		HTTP_NOT_FOUND, INVALID_SERVER_VERSION, SSL_REJECTED, OTHER_ERROR, IO_ERROR, INVALID_SERVER_URL, HTTP_SERVER_ERROR,
		ERROR_PARSING_CONNECTIONS, ERROR_PARSING_CHANNELS
	}

	protected ErrorType m_lastError;
	protected SharedPreferences m_prefs;
	protected String m_requestUrl;
	protected Response m_response;
	protected String m_sessionId;

	public HttpRequestTask(Context context, String requestUrl, String sessionId) {
		m_prefs = PreferenceManager.getDefaultSharedPreferences(context);
		m_requestUrl = requestUrl;
		m_sessionId = sessionId;
	}

	@SuppressWarnings("unchecked")
	public void execute(HashMap<String, String> map) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			super.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, map);
		else
			super.execute(map);
	}

	public static int getErrorMessage(ErrorType code) {
		switch (code) {
			case NOT_CONFIGURED:
				return R.string.login_need_configure;
			case NOT_AUTHORIZED:
				return R.string.login_failed;
			case HTTP_ERROR:
				return R.string.error_http_error;
			case NEED_UPDATE:
				return R.string.error_need_update;
			case HTTP_FORBIDDEN:
				return R.string.error_http_forbidden;
			case PARSE_ERROR:
				return R.string.error_parse_error;
			case HTTP_NOT_FOUND:
				return R.string.error_http_not_found;
			case INVALID_SERVER_VERSION:
				return R.string.incorrect_server_version;
			case SSL_REJECTED:
				return R.string.error_ssl_rejected;
			/* case OTHER_ERROR: */
			case IO_ERROR:
				return R.string.error_io_error;
			case INVALID_SERVER_URL:
				return R.string.error_invalid_url;
			case ERROR_PARSING_CHANNELS:
				return R.string.error_parsing_channels;
			case ERROR_PARSING_CONNECTIONS:
				return R.string.error_parsing_connections;
			case HTTP_SERVER_ERROR:
			default:
				return R.string.error_other_error;
		}
	}

	@Override
	protected JsonElement doInBackground(HashMap<String, String>... params) {
		FormBody.Builder bodyBuilder = new FormBody.Builder();

		for (String key : params[0].keySet()) {
			bodyBuilder.add(key, params[0].get(key));
        }

		Request.Builder reqBuilder = new Request.Builder()
				.url(m_requestUrl)
                .addHeader("Cookie", "ttirc_sid2=" + m_sessionId)
				.post(bodyBuilder.build());

		if (m_prefs.getBoolean("transport_debugging", false)) {
			Log.d(TAG, ">>> " + bodyBuilder.build());
		}

		String httpLogin = m_prefs.getString("http_login", "").trim();
		String httpPassword = m_prefs.getString("http_password", "").trim();

		if (httpLogin.length() > 0) {
			reqBuilder.addHeader("Authorization", Credentials.basic(httpLogin, httpPassword));
		}

		OkHttpClient client = new OkHttpClient();

		try {
			m_response = client
					.newCall(reqBuilder.build())
					.execute();

			String respBody = m_response.body().string();

			if (m_prefs.getBoolean("transport_debugging", false)) {
				Log.d(TAG, "<<< " + m_response.code() + ": " +  respBody);
			}

			if (m_response.isSuccessful()) {

				JsonParser parser = new JsonParser();

				JsonElement result = parser.parse(respBody);

				if (result.isJsonObject()) {
					JsonObject resultObj = result.getAsJsonObject();
					JsonElement error = resultObj.get("error");

					if (error != null) {
						switch (error.getAsInt()) {
							case Constants.ERR_NOT_AUTHORIZED:
								//m_lastError = ErrorType.NOT_AUTHORIZED;
								m_lastError = ErrorType.HTTP_ERROR;
								break;
							case Constants.ERR_NEED_UPDATE:
								m_lastError = ErrorType.NEED_UPDATE;
								break;
							default:
								m_lastError = ErrorType.OTHER_ERROR;
						}
						return null;
					}
				}

				return result;

			} else {

				switch (m_response.code()) {
					case 401:
						m_lastError = ErrorType.NOT_AUTHORIZED;
						break;
					case 403:
						m_lastError = ErrorType.HTTP_FORBIDDEN;
						break;
					case 404:
						m_lastError = ErrorType.HTTP_NOT_FOUND;
						break;
					case 500:
					case 501:
						m_lastError = ErrorType.HTTP_SERVER_ERROR;
						break;
					default:
						m_lastError = ErrorType.OTHER_ERROR;
						break;
				}
			}
		} catch (javax.net.ssl.SSLHandshakeException e) {
			m_lastError = ErrorType.SSL_REJECTED;
			e.printStackTrace();
		} catch (javax.net.ssl.SSLPeerUnverifiedException e) {
			m_lastError = ErrorType.SSL_REJECTED;
			e.printStackTrace();
		} catch (IOException e) {
			m_lastError = ErrorType.IO_ERROR;
			e.printStackTrace();
		} catch (com.google.gson.JsonSyntaxException e) {
			m_lastError = ErrorType.PARSE_ERROR;
			e.printStackTrace();
		} catch (Exception e) {
			m_lastError = ErrorType.OTHER_ERROR;
			e.printStackTrace();
		}

		return null;
	}
}
