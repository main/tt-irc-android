package org.fox.ttirc;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.fox.ttirc.types.Emoticon;

import java.util.ArrayList;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import ir.mirrajabi.searchdialog.core.BaseFilter;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.FilterResultListener;
import ir.mirrajabi.searchdialog.core.OnPerformFilterListener;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

/**
 * Created by MADNESS on 5/14/2017.
 */

public class EmoticonSearchDialog<T extends Emoticon> extends BaseSearchDialogCompat<T> {
    private static final int MAX_RESULTS = 30;

    private String mTitle;
    private String mSearchHint;
    private SearchResultListener<T> mSearchResultListener;

    private TextView mTxtTitle;
    private EditText mSearchBox;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;

    // In case you are doing process in another thread
    // and wanted to update the progress in that thread
    private Handler mHandler;
    private Context mContext;

    private ArrayList<T> mInitialItems;

    public EmoticonSearchDialog(
            Context context, String title, String searchHint,
            @Nullable Filter filter, ArrayList<T> items, ArrayList<T> initialItems,
            SearchResultListener<T> searchResultListener, int theme
    ) {
        super(context, items, filter, null, null, theme);
        init(context, title, searchHint, searchResultListener, initialItems);
    }

    private void init(
            Context context, String title, String searchHint,
            final SearchResultListener<T> searchResultListener, ArrayList<T> initialItems
    ) {
        mContext = context;
        mTitle = title;
        mSearchHint = searchHint;
        mSearchResultListener = searchResultListener;
        mInitialItems = initialItems;
        setFilterResultListener(new FilterResultListener<T>() {
            @Override
            public void onFilter(ArrayList<T> items) {
                ArrayList<T> results = resultSubset(items);

                ((EmoticonSearchDialogAdapter) getAdapter())
                        .setSearchTag(mSearchBox.getText().toString())
                        .setItems(results);
            }
        });
        mHandler = new Handler();
    }

    private ArrayList<T> resultSubset(ArrayList<T> items) {
        return items.size() > MAX_RESULTS ? new ArrayList<>(items.subList(0, MAX_RESULTS)) : items;
    }

    @Override
    protected void getView(View view) {
        setContentView(view);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setCancelable(true);
        mTxtTitle = view.findViewById(R.id.txt_title);
        mSearchBox = view.findViewById(getSearchBoxId());
        mRecyclerView = view.findViewById(getRecyclerViewId());
        mProgressBar = view.findViewById(R.id.progress);
        mTxtTitle.setText(mTitle);

        if (mTitle.length() == 0)
            mTxtTitle.setVisibility(View.GONE);

        mSearchBox.setHint(mSearchHint);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setVisibility(View.GONE);
        view.findViewById(R.id.dummy_background)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dismiss();
                    }
                });
        final EmoticonSearchDialogAdapter adapter = new EmoticonSearchDialogAdapter<>(mContext,
                R.layout.emoticons_row, resultSubset(mInitialItems != null ? mInitialItems : getItems())
        );
        adapter.setSearchResultListener(mSearchResultListener);
        adapter.setSearchDialog(this);
        setFilterResultListener(getFilterResultListener());
        setAdapter(adapter);
        mSearchBox.requestFocus();
        ((BaseFilter<T>) getFilter()).setOnPerformFilterListener(new OnPerformFilterListener() {
            @Override
            public void doBeforeFiltering() {
                setLoading(true);
            }

            @Override
            public void doAfterFiltering() {
                setLoading(false);
            }
        });
    }

    public EmoticonSearchDialog setSearchHint(String searchHint) {
        mSearchHint = searchHint;
        if (mSearchBox != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mSearchBox.setHint(mSearchHint);
                }
            });
        }
        return this;
    }

    public void setLoading(final boolean isLoading) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mProgressBar != null) {
                    mRecyclerView.setVisibility(!isLoading ? View.VISIBLE : View.GONE);
                }
                if (mRecyclerView != null) {
                    mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                }
            }
        });
    }

    public EmoticonSearchDialog setSearchResultListener(
            SearchResultListener<T> searchResultListener
    ) {
        mSearchResultListener = searchResultListener;
        return this;
    }

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.search_dialog_compat;
    }

    @IdRes
    @Override
    protected int getSearchBoxId() {
        return R.id.txt_search;
    }

    @IdRes
    @Override
    protected int getRecyclerViewId() {
        return R.id.rv_items;
    }

    public EditText getSearchBox() {
        return mSearchBox;
    }

    public String getTitle() {
        return mTitle;
    }

    public EmoticonSearchDialog setTitle(String title) {
        mTitle = title;
        if (mTxtTitle != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mTxtTitle.setText(mTitle);
                }
            });
        }
        return this;
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public ProgressBar getProgressBar() {
        return mProgressBar;
    }

    public TextView getTitleTextView() {
        return mTxtTitle;
    }
}