package org.fox.ttirc;

public interface ChannelView {
	String getChannel();
	int getConnectionId();
    void appendText(String text);
    int getMessagesCount();
}
