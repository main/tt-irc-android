package org.fox.ttirc.backend;

import android.os.Bundle;

import com.google.gson.JsonElement;

import org.fox.ttirc.net.HttpRequestTask;
import org.fox.ttirc.types.Channel;
import org.fox.ttirc.types.Connection;
import org.fox.ttirc.types.Emoticon;
import org.fox.ttirc.types.Message;

import java.util.List;
import java.util.Map;


public interface UpdateServiceApi {
		interface OnSendCompleted {
			void onSendCompleted(JsonElement result);
		}
		void addListener(String ident, UpdateServiceListener listener);
		void removeListener(String ident);
		List<Connection> getConnections();
		Map<String, Channel> getChannels(int connectionId);
		List<Message> getMessages(int connectionId, String channel, int sincePos);
		String getUserHosts(int connectionId);
		List<String> getNicks(int connectionId, String channel);
		String getTopic(int connectionId, String channel);
		String getSelfNick(int connectionId);
		HttpRequestTask.ErrorType getLastError();
		String getCsrfToken();
		void requestHistory(int connectionid, String channel, int offset);
		String getSessionId();
		int getConnectionStatus(int connectionId);
		int getListenerCount();
		boolean loggedIn();
		Bundle getCounters(int connectionId, String channel);
		void setActiveChannel(int connectionId, String channel);
		void expireMessages(int connectionId, String channel, List<Message> remove);
		//void setSessionId(String sessionId);
		void send(int connectionId, String channel, String message, UpdateServiceApi.OnSendCompleted completed);
		Map<String, Emoticon> getEmoticons(boolean favoriteOnly);
        boolean isInitMode();
}
