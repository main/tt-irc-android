package org.fox.ttirc.backend;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.here.oksse.OkSse;
import com.here.oksse.ServerSentEvent;

import org.fox.ttirc.BuildConfig;
import org.fox.ttirc.CommonActivity;
import org.fox.ttirc.MainActivity;
import org.fox.ttirc.R;
import org.fox.ttirc.SendTask;
import org.fox.ttirc.net.HttpRequestTask;
import org.fox.ttirc.net.HttpRequestTask.ErrorType;
import org.fox.ttirc.types.Channel;
import org.fox.ttirc.types.Connection;
import org.fox.ttirc.types.Constants;
import org.fox.ttirc.types.Emoticon;
import org.fox.ttirc.types.Message;
import org.fox.ttirc.types.Options;
import org.fox.ttirc.utils.GetFileService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

public class UpdateService extends Service {
	private final String TAG = this.getClass().getSimpleName();
	private static final String CONNSTATE_PERSIST_NAME = "connstate.object-";
	private static final String EMOTICONS_PERSIST_NAME = "emoticons.object";

	private static final int HIST_REQ_READY = 0;
	private static final int HIST_REQ_IN_PROGRESS = 1;
	private static final int HIST_REQ_IN_COMPLETE = 2;

	private static final int DEFAULT_BUF_SIZE = 128;

	private static UpdateService m_instance = null;

	private String m_sessionId;
	private String m_csrfToken;
	private AtomicInteger m_lastId = new AtomicInteger(-1);
	private AtomicBoolean m_initMode = new AtomicBoolean(true);
	private ErrorType m_lastError;
	private List<Integer> m_idHistory = new ArrayList<>();

	private int m_activeConnection;
	private String m_activeChannel;
	@SuppressLint("SimpleDateFormat")
	private DateFormat m_dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Date m_serviceStarted;

	private SharedPreferences m_prefs;

	private List<Connection> m_connections;

	private Options m_options;
	private HashMap<Integer, HashMap<String, Channel>> m_channels;
	private HashMap<Integer, HashMap<String, List<Message>>> m_messageMap = new HashMap<>();
	private HashMap<Integer, HashMap<String, Integer>> m_numUnreadMap = new HashMap<>();
	private HashMap<Integer, HashMap<String, Boolean>> m_topicEventMap = new HashMap<>();
	private HashMap<Integer, HashMap<String, Integer>> m_historyCompleteMap = new HashMap<>();
	private HashMap<String, Emoticon> m_emoticons = new HashMap<>();

	private Gson m_gson = new Gson();

	protected AtomicBoolean m_loggedIn = new AtomicBoolean(true);

	HashMap<String, UpdateServiceListener> m_listeners = new HashMap<String, UpdateServiceListener>();

	private AtomicBoolean m_runnableEnabled = new AtomicBoolean(true);

	private final Object m_updateThreadLock = new Object();
	private Timer m_updateThreadMonitor;
	private Thread m_updateRunnableThread;
	private Runnable m_updateRunnable = new Runnable() {

		private ServerSentEvent m_sse;
		private ServerSentEvent.Listener m_sseListener = new ServerSentEvent.Listener() {
			private final String TAG = UpdateService.this.TAG + ":SSEListener";
			private Response m_lastResponse = null;

			@Override
			public void onOpen(ServerSentEvent sse, Response response) {
				Log.d(TAG, "onOpen: A" + response.toString());
				m_lastResponse = response;
			}

			@Override
			public void onMessage(ServerSentEvent sse, String id, String event, String message) {
				Log.d(TAG, "onMessage: E: " + event + " M: " + message);

				if ("update".equals(event)) {
					JsonParser parser = new JsonParser();
					final JsonElement result = parser.parse(message);

					new Handler(Looper.getMainLooper()).post(new Runnable() {
						public void run() {
							parseReceived(result);
						}
					});
				}
			}

			@Override
			public void onComment(ServerSentEvent sse, String comment) {
				//
			}

			@Override
			public boolean onRetryTime(ServerSentEvent sse, long milliseconds) {
				return true;
			}

			@Override
			public boolean onRetryError(ServerSentEvent sse, Throwable throwable, Response response) {
				return true;
			}

			@Override
			public void onClosed(ServerSentEvent sse) {
				Log.d(TAG, "onClosed");

				m_requestInProgress = false;

				try {
					JsonParser parser = new JsonParser();
					JsonElement result = parser.parse(m_lastResponse.body().string());

					if (result.isJsonObject()) {
						JsonObject resultObj = result.getAsJsonObject();
						JsonElement error = resultObj.get("error");

						if (error != null) {
							switch (error.getAsInt()) {
								case Constants.ERR_NOT_AUTHORIZED:
									Log.d(TAG, "sse listener: lost session id");

									synchronized (m_updateThreadLock) {
										m_lastError = ErrorType.NOT_AUTHORIZED;
										m_sessionId = null;
									}

									m_loggedIn.set(false);

									break;
								case Constants.ERR_NEED_UPDATE:
									synchronized (m_updateThreadLock) {
										m_lastError = ErrorType.NEED_UPDATE;
									}
									break;
								default:
									synchronized (m_updateThreadLock) {
										m_lastError = ErrorType.OTHER_ERROR;
									}
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			@Override
			public Request onPreRetry(ServerSentEvent sse, Request originalRequest) {
				return null;
			}
		};

		private void initEventStream() {
			Log.d(TAG, "creating event stream, sid=" + m_sessionId);

			if (m_sse != null)
				m_sse.close();

			String ttircUrl = m_prefs.getString("ttirc_url", "").trim() + "/backend.php?op=evtsource";

			HttpUrl.Builder updateUrlBuilder = HttpUrl.parse(ttircUrl).newBuilder();

			updateUrlBuilder
					.addQueryParameter("last_id", String.valueOf(m_lastId))
					.addQueryParameter("bufsize", String.valueOf(DEFAULT_BUF_SIZE));

			if (m_options != null && m_options.uniqid != null)
				updateUrlBuilder.addQueryParameter("uniqid", m_options.uniqid);

			Request request = new Request.Builder()
					.addHeader("Cookie", "ttirc_sid2=" + m_sessionId)
					.url(updateUrlBuilder.build())
					.build();

			OkSse okSse = new OkSse();
			m_sse = okSse.newServerSentEvent(request, m_sseListener);
		}

		private boolean m_requestInProgress = false;

		@Override
		public void run() {

			while (m_runnableEnabled.get()) {

				if (BuildConfig.DEBUG)
					Log.d(TAG, "inProg=" + m_requestInProgress + " init mode=" + m_initMode + " sid=" + m_sessionId + " wrk=" + m_runnableEnabled.get());

				if (!m_requestInProgress && m_sessionId != null && m_loggedIn.get()) {
					m_requestInProgress = true;

					if (m_initMode.get()) {
						final String ttircUrl = m_prefs.getString("ttirc_url", "").trim();

						final HashMap<String, String> params = new HashMap<>();

						synchronized (m_updateThreadLock) {
							params.put("csrf_token", m_csrfToken);
						}

						params.put("no_pong", "true");
						params.put("update_delay", "40");
						params.put("bufsize", String.valueOf(DEFAULT_BUF_SIZE));
						params.put("init", "true");
						params.put("op", "update");

						if (m_lastId.get() != -1)
							params.put("last_id", String.valueOf(m_lastId));

						if (m_options != null && m_options.uniqid != null)
							params.put("uniqid", m_options.uniqid);

						@SuppressLint("StaticFieldLeak") HttpRequestTask task = new HttpRequestTask(getApplicationContext(),
								ttircUrl + "/backend.php", m_sessionId) {
							@Override
							protected void onPostExecute(JsonElement result) {
								if (result != null) {
									synchronized (m_updateThreadLock) {
										UpdateService.this.m_lastError = ErrorType.NO_ERROR;
									}

									// this might set m_lastError with non-http related stuff
									parseReceived(result);

									initEventStream();

									updateEmoticons(ttircUrl, params);

								} else {
									synchronized (m_updateThreadLock) {
										UpdateService.this.m_lastError = m_lastError;

										if (m_lastError == ErrorType.NOT_AUTHORIZED) {
											Log.d(TAG, "backend: lost session ID");

											m_loggedIn.set(false);

											m_sessionId = null;
											m_requestInProgress = false;
										}
									}
								}

								notifyListeners();

								m_initMode.set(false);
							}
						};

						task.execute(params);

					} else {
						initEventStream();
					}
				}

				try {
					Thread.sleep(3000L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		}

		private void updateEmoticons(String ttircUrl, HashMap<String, String> params) {

			Log.d(TAG, "updating emoticons...");

			@SuppressLint("StaticFieldLeak") HttpRequestTask etask = new HttpRequestTask(getApplicationContext(),
					ttircUrl.trim() + "/backend.php", m_sessionId) {
				@Override
				protected void onPostExecute(JsonElement result) {
					if (result != null) {
						try {
							Type listType = new TypeToken<HashMap<String, HashMap<String, Emoticon>>>() {}.getType();

							HashMap<String, HashMap<String, Emoticon>> emoticons = m_gson.fromJson(result, listType);

							Log.d(TAG, "emoticons keyset=" + emoticons.keySet().size());

							for (String section : emoticons.keySet()) {
								for (String key : emoticons.get(section).keySet()) {
									//Log.d(TAG, "emoticons: processing: " + key);

									Emoticon emot = emoticons.get(section).get(key);

									if (emot.file != null) {

										File imageFile = downloadEmoticon(emot);

										emot.localfile = imageFile.getAbsolutePath();
										emot.name = key;

										m_emoticons.put(key, emot);
									}
								}
								if (m_response.header("X-Map-Last-Modified") != null) {
									SharedPreferences.Editor editor = m_prefs.edit();
									editor.putString("emoticons_last_modified", m_response.header("X-Map-Last-Modified"));
									editor.apply();

									try {
										File file = new File(getCacheDir(), EMOTICONS_PERSIST_NAME);
										ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
										oos.writeObject(m_emoticons);
										oos.close();
									} catch (Exception e) {
										e.printStackTrace();
									}
								}

								notifyListeners();
							}

							if (emoticons.keySet().size() == 0 && m_emoticons.keySet().size() != 0) {
								for (String key : m_emoticons.keySet()) {
									Emoticon emot = m_emoticons.get(key);

									//Log.d(TAG, "checking emoticon: " + key);

									if (emot.file != null) {
										downloadEmoticon(emot);
									}
								}
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			};

			params.put("op", "emoticons");

			if (m_emoticons.keySet().size() != 0) {
				params.put("modified", m_prefs.getString("emoticons_last_modified", ""));
			}

			etask.execute(params);
		}

		private File downloadEmoticon(Emoticon emot) {

			String baseUrl = m_prefs.getString("ttirc_url", "").trim();
			String fileName = emot.file.replace("/", "__");
			File storage = getCacheDir();

			//Log.d(TAG, "filename=" + fileName);

			File imageFile = new File(storage, fileName);

			if (!imageFile.exists()) {
				String fetchUrl = baseUrl + "/emoticons/" + emot.file;

				Intent intent = new Intent(UpdateService.this,
						GetFileService.class);

				intent.putExtra("outputFile", imageFile.getAbsolutePath());
				intent.putExtra("fetchUrl", fetchUrl);
				intent.putExtra("httpLogin", m_prefs.getString("http_login", "").trim());
				intent.putExtra("httpPassword", m_prefs.getString("http_password", "").trim());

				JobIntentService.enqueueWork(UpdateService.this, GetFileService.class, 0, intent);
			}

			return imageFile;
		}
	};

	private void notifyListeners() {
		for (UpdateServiceListener listener : m_listeners.values()) {
			try {
				listener.handleUpdate();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
	}

	private IBinder m_apiEndpoint = new LocalBinder();
	
	public class LocalBinder extends Binder implements UpdateServiceApi {

		@Override
		public void addListener(String ident, UpdateServiceListener listener) {
			Log.d(TAG, "Adding listener: " + ident);
			m_listeners.put(ident, listener);
		}

		@Override
		public void removeListener(String ident) {
			Log.d(TAG, "Removing listener: " + ident);
			m_listeners.remove(ident);
		}

		@Override
		public int getListenerCount() {
			return m_listeners.size();
		}
		
		@Override
		public List<Connection> getConnections() {
			return m_connections;
		}

		@Override
		public Map<String, Channel> getChannels(int connectionId) {
			return m_channels.get(connectionId);
		}

		@Override
		public void expireMessages(int connectionId, String channel, List<Message> remove) {
			List<Message> messages = m_messageMap.get(connectionId).get(channel);

			for (Message msg : remove)
				messages.remove(msg);
		}

		@Override
		public List<Message> getMessages(int connectionId, String channel, int sincePos) {
			try {
				List<Message> messages = Objects.requireNonNull(m_messageMap.get(connectionId)).get(channel);

				//Log.d(TAG, "getMessages:" + connectionId + " " + channel + " sincePos:" + sincePos + " Size:" + messages.size());

				if (sincePos > 0)
					return new ArrayList<>(messages.subList(sincePos, messages.size()));
				else
					return new ArrayList<>(messages);

			} catch (NullPointerException e) {	
				// no messages for this channel
			}

			return null;
		}

		@Override
		public String getUserHosts(int connectionId) {
			try {
				return m_gson.toJson(findConnectionById(connectionId).userhosts);
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		public List<String> getNicks(int connectionId, String channel) {
			try {
				return m_channels.get(connectionId).get(channel).users;
			} catch (NullPointerException e) {
				// no nicks for this channel
				return null;
			}		
		}		

		@Override
		public String getTopic(int connectionId, String channel) {
			try {
				if (channel.equals("---")) {
					String topic = null;

					Connection conn = findConnectionById(connectionId);

					switch (conn.status) {
					case Constants.CS_DISCONNECTED:
						topic = getString(R.string.disconnected);
						break;
					case Constants.CS_CONNECTING:
						topic = getString(R.string.connecting);
						break;
					case Constants.CS_CONNECTED:
						topic = getString(R.string.connected, conn.active_server);
						break;
					}

					return topic;
				} else {
					return m_channels.get(connectionId).get(channel).topic.get(0);
				}
			} catch (NullPointerException e) {
				return null;
			}
		}

		@Override
		public String getSelfNick(int connectionId) {
			try {
				return findConnectionById(connectionId).active_nick;
			} catch (NullPointerException e) {
				return null;
			}
		}

		@Override
		public ErrorType getLastError() {
			return m_lastError;
		}

		@Override
		public int getConnectionStatus(int connectionId) {
			try {
				return findConnectionById(connectionId).status;
			} catch (NullPointerException e) {
				return 0;
			}
		}

		@Override
		public boolean loggedIn() {
			return m_loggedIn.get();
		}

		@Override
		public String getCsrfToken() {
			synchronized (m_updateThreadLock) {
				return m_csrfToken;
			}
		}

		@Override
		public String getSessionId() {
			synchronized (m_updateThreadLock) {
				return m_sessionId;
			}
		}

		@Override
		public void setActiveChannel(int connectionId, String channel) {
			m_activeConnection = connectionId;
			m_activeChannel = channel;

			setUnread(connectionId, channel, 0);

			notifyListeners();
		}

		@Override
		public Bundle getCounters(int connectionId, String channel) {
			Bundle bundle = new Bundle();

			bundle.putInt("unread", UpdateService.this.getUnread(connectionId, channel));
			//bundle.putInt("highlighted", UpdaterService.this.getHighlighted(connectionId, channel));

			return bundle;
		}

		@Override
		public void send(int connectionId, String channel, String message, final UpdateServiceApi.OnSendCompleted completed) {
			@SuppressLint("StaticFieldLeak") SendTask task = new SendTask(getApplicationContext(), connectionId, channel, getSessionId(), getCsrfToken()) {
				protected void onPostExecute(JsonElement result) {
					if (result != null) {
						UpdateService.this.m_lastError = ErrorType.NO_ERROR;

						// NOTE: backend no longer sends lines on send
						// this might set m_lastError with non-http related stuff
						//parseReceived(result);

					} else {
						UpdateService.this.m_lastError = m_lastError;
					}

					notifyListeners();

					if (completed != null)
						completed.onSendCompleted(result);
				}
			};
			
			task.execute(message);
		}

		@Override
		public Map<String, Emoticon> getEmoticons(boolean favoriteOnly) {
			if (!favoriteOnly) {
				return m_emoticons;
			} else {
				HashMap<String, Emoticon> tmp = new HashMap<>();

				if (m_options.emoticons_favorite != null && m_options.emoticons_favorite.size() > 0) {
					for (String fav : m_options.emoticons_favorite) {
						if (m_emoticons.containsKey(fav))
							tmp.put(fav, m_emoticons.get(fav));

					}
				}

				return tmp;
			}
		}

        @Override
        public boolean isInitMode() {
            return m_initMode.get();
        }

		@Override
		public void requestHistory(final int connectionId, final String channel, final int offset) {
			Log.d(TAG, "requesting history for: " + channel + "; offset=" + offset);

			HashMap<String, Integer> list = m_historyCompleteMap.get(connectionId);

			if (list == null) {
				list = new HashMap<>();
				list.put(channel, HIST_REQ_READY);
				m_historyCompleteMap.put(connectionId, list);
			}

			try {
				switch (list.get(channel)) {
					case HIST_REQ_IN_PROGRESS:
						Log.d(TAG, "history request in progress for " + channel);
						return;
					case HIST_REQ_IN_COMPLETE:
						Log.d(TAG, "history already completed for " + channel);
						return;
				}
			} catch (Exception e) {
				//
			}

			list.put(channel, HIST_REQ_IN_PROGRESS);

			HashMap<String, String> params = new HashMap<>();

			synchronized (m_updateThreadLock) {
				params.put("csrf_token", m_csrfToken);
			}

			params.put("op", "history");
			params.put("offset", String.valueOf(offset));
			params.put("connection", String.valueOf(connectionId));
			params.put("chan", channel);

			final String ttircUrl = m_prefs.getString("ttirc_url", "").trim();

			@SuppressLint("StaticFieldLeak") HttpRequestTask task = new HttpRequestTask(getApplicationContext(),
					ttircUrl + "/backend.php", m_sessionId) {
				@Override
				protected void onPostExecute(JsonElement result) {
					try {

						JsonArray array = result.getAsJsonArray();
						Gson gson = new Gson();

						Type listType = new TypeToken<List<Message>>() {
						}.getType();
						List<Message> messages = gson.fromJson(array.get(1), listType);

						Log.d(TAG, "history, got " + messages.size() + " lines; buf=" + m_options.default_buffer_size);

						HashMap<String, Integer> list = m_historyCompleteMap.get(connectionId);

						if (m_options.default_buffer_size > 0 && messages.size() == m_options.default_buffer_size) {

							new Handler().postDelayed(new Runnable() {
								@Override
								public void run() {
									requestHistory(connectionId, channel, offset + m_options.default_buffer_size);
								}
							}, 1000);

							list.put(channel, HIST_REQ_READY);
							Log.d(TAG, "history request finished for " + channel);

						} else {
							list.put(channel, HIST_REQ_IN_COMPLETE);
							Log.d(TAG, "history complete for " + channel);
						}

						parseReceived(result);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};

			task.execute(params);
		}
    }

	@Override
	public IBinder onBind(Intent intent) {		
		Log.d(TAG, "Bound by intent " + intent);

		if (intent.getExtras() != null) {
			synchronized (m_updateThreadLock) {
				m_sessionId = intent.getStringExtra("sessionId");
				m_csrfToken = intent.getStringExtra("csrfToken");
			}
		}
		
		return m_apiEndpoint;
	}
	
	public static boolean isInstanceCreated() { 
	   	return m_instance != null;
	}
	
	protected Connection findConnectionById(int connectionId) {
		for (Connection conn : m_connections) {
			if (conn.id == connectionId)
				return conn;					
		}
		return null;
	}

	protected boolean parseReceived(JsonElement result) {
		try {
			if (result != null && result.isJsonArray()) {
				Gson gson = new Gson();

				JsonArray array = result.getAsJsonArray();

				Type listType = new TypeToken<List<Connection>>() {}.getType();

				try {
					JsonElement maybeDuplicate = array.get(0);

					if (maybeDuplicate.isJsonObject() && maybeDuplicate.getAsJsonObject().get("duplicate") != null) {
						//Log.d(TAG, "m_connections=duplicate");
					} else {
						m_connections = gson.fromJson(array.get(0), listType);
					}
				} catch (JsonParseException e) {
					Log.d(TAG, "parseReceived: error parsing connections object");
					e.printStackTrace();

					m_lastError = ErrorType.ERROR_PARSING_CONNECTIONS;
					return false;
				}

				// If connection is disconnected, cleanup local data for it
				for (Connection conn : m_connections) {
					if (conn.status == Constants.CS_DISCONNECTED) {
						m_topicEventMap.remove(conn.id);
						m_messageMap.remove(conn.id);
						m_numUnreadMap.remove(conn.id);
						//m_numHighlightMap.remove(conn.id);
					}
				}

				listType = new TypeToken<HashMap<Integer, HashMap<String, Channel>>>() {}.getType();

				try {
					JsonElement maybeDuplicate = array.get(2);
					if (!maybeDuplicate.isJsonObject() || maybeDuplicate.getAsJsonObject().get("duplicate") == null) {
						m_channels = gson.fromJson(array.get(2), listType);

						try {
							File file = new File(getCacheDir(), CONNSTATE_PERSIST_NAME + m_prefs.getString("login", ""));
							ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
							oos.writeObject(m_connections);
							oos.writeObject(m_channels);
							oos.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} catch (JsonParseException e) {
					Log.d(TAG, "parseReceived: error parsing channels object");
					e.printStackTrace();

					m_lastError = ErrorType.ERROR_PARSING_CHANNELS;
					return false;
				}

				m_lastError = ErrorType.NO_ERROR;

				notifyListeners();

				for (int connection : m_channels.keySet()) {
					HashMap<String, List<Message>> map = m_messageMap.get(connection);

					for (String channel : m_channels.get(connection).keySet()) {
						map = m_messageMap.get(connection);

						if (map == null) {
							map = new HashMap<>();
							m_messageMap.put(connection, map);
						}

						List<Message> list = map.get(channel);

						if (list == null) {
							list = new LinkedList<>();
							map.put(channel, list);
						}

						List<String> topic = m_channels.get(connection).get(channel).topic;

						if (topic != null && topic.size() == 3 && !topic.get(0).equals("")) {
							//Log.d(TAG, "Topic for " + channel + ":" + topic.toString());

							HashMap<String, Boolean> eventList = m_topicEventMap.get(connection);

							if (eventList == null) {
								eventList = new HashMap<String, Boolean>();
								m_topicEventMap.put(connection, eventList);
							}

							if (eventList.get(channel) == null) {
								eventList.put(channel, true);
								Log.d(TAG, "Synthesizing topic events for " + channel);
								synthesizeTopicMessage(list, connection, channel);
							}
						}
					}
				}

				listType = new TypeToken<List<Message>>() {}.getType();
				List<Message> messages = gson.fromJson(array.get(1), listType);

				for (Message message : messages) {
					int connectionId = message.connection_id;
					String channel = message.channel;

					// filter out SERVER_PONG events
					if (message.message_type == Constants.MSGT_EVENT &&
							message.message != null && message.message.indexOf("SERVER_PONG:") == 0) {
						continue;
					}

					if (m_idHistory.size() > 2048 + 100)
						m_idHistory.subList(0, 100).clear();

					if (!m_idHistory.contains(message.id)) {
						m_lastId.set(pushMessage(connectionId, channel, message));
						m_idHistory.add(message.id);
					}
				}

				try {
					JsonElement maybeDuplicate = array.get(3);
					if (!maybeDuplicate.isJsonObject() || maybeDuplicate.getAsJsonObject().get("duplicate") == null) {
						m_options = gson.fromJson(array.get(3), Options.class);

						//Log.d(TAG, "got sid=" + m_options.sid);

						// reassign session ID if provided
						if (m_options.sid != null) {
							synchronized (m_updateThreadLock) {
								m_sessionId = m_options.sid;
							}
						}
					}
				} catch (JsonParseException e) {
					Log.d(TAG, "parseReceived: error parsing options object");
					e.printStackTrace();
					return false;
				}

				notifyListeners();

				return true;
			} else {
				Log.d(TAG, "got malformed update object:" + result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		notifyListeners();

		return false;		
	}

	private int pushMessage(int connectionId, String channel, Message message) {
		HashMap<String, List<Message>> map = m_messageMap.get(connectionId);

		try {			
			boolean notifyOnChanmsg = m_prefs.getBoolean("notify_on_chanmsg", false);
			boolean notifyOnHighlight = m_prefs.getBoolean("notify_on_highlight", false);
			boolean notifyOnPrivmsg = m_prefs.getBoolean("notify_on_privmsg", false);
			boolean notifyOnNotice = m_prefs.getBoolean("notify_on_notice", false);

			String[] highlightAdditional = m_prefs.getString("highlight_additional", "").split(",");

			//Log.d(TAG, "[IN MSG] " + message.id + " " + message.channel + " " + message.message);
		
			if (map == null) {
				map = new HashMap<>();
				m_messageMap.put(connectionId, map);
			}
	
			List<Message> list = map.get(channel);
	
			if (list == null) {
				list = new LinkedList<>();
				map.put(channel, list);
			}
	
			list = m_messageMap.get(connectionId).get(channel);
			
			try {
				message.date = m_dateFormatter.parse(message.ts);
			} catch (ParseException e) {
				message.date = new Date();
			} 

			// channel fragment deals with the sorting and expiration
			list.add(message);

			if (m_serviceStarted.compareTo(message.date) < 0 && !channel.equals("---") && !m_initMode.get()) {
				if (message.message_type != Constants.MSGT_EVENT &&
						(notifyOnNotice || message.message_type != Constants.MSGT_NOTICE)) {
	
					if (!(connectionId == m_activeConnection && channel.equals(m_activeChannel)))
						setUnread(connectionId, channel, getUnread(connectionId, channel)+1);
	
					boolean highlighted = false;
					boolean inActiveChannel = m_listeners.size() > 0 && connectionId == m_activeConnection && channel.equals(m_activeChannel);

					if (notifyOnHighlight) {
						highlighted = message.message.contains(findConnectionById(connectionId).active_nick) &&
							!(connectionId == m_activeConnection && channel.equals(m_activeChannel));

						if (!highlighted && highlightAdditional.length > 0) {
							for (String hl : highlightAdditional) {
								if (hl.length() > 0 && message.message.contains(hl) && !inActiveChannel) {
									highlighted = true;
									break;
								}
							}
						}
					}

//					for (String s : m_listeners.keySet())
//						Log.d(TAG, "Current listener: " + s);

					boolean smallScreenMode = m_prefs.getBoolean("small_screen_mode", false);

//					Log.d(TAG, "msgNotify: numL=" + m_listeners.size() + " isHL:" + highlighted + " isAct=" + inActiveChannel +
//							", CONN:" + connectionId + "=" + m_activeConnection + "; [" + channel + "=" + m_activeChannel + "]");

					NotificationManager nmgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

					if (!findConnectionById(connectionId).active_nick.equals(message.sender) &&
							!inActiveChannel && (m_listeners.size() == 0 || smallScreenMode || highlighted)) {

						String notifyTitle = null;
						String notifyBody = getString(R.string.msg_notify_body, message.message);

						boolean priorityNotification = highlighted;

						if (channel.indexOf('#') == 0 && (notifyOnChanmsg || highlighted)) { // channel notification
							notifyTitle = getString(R.string.msg_notify_title_channel, message.sender, channel);
						} else if (channel.indexOf('#') != 0 && (notifyOnPrivmsg || highlighted)) { // privmsg notification
							notifyTitle = getString(R.string.msg_notify_title_private, message.sender);
							priorityNotification = true;
						}

						if (notifyTitle != null) {

                            Intent intent = new Intent(this, MainActivity.class);
                            intent.putExtra("channel", channel);
                            intent.putExtra("connectionId", connectionId);

                            PendingIntent contentIntent = PendingIntent.getActivity(this, Constants.NOTIFY_UNREAD,
                                    intent, PendingIntent.FLAG_UPDATE_CURRENT);

                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
									priorityNotification ? CommonActivity.NOTIFICATION_CHANNEL_PRIORITY : CommonActivity.NOTIFICATION_CHANNEL_GROUP)
                                .setContentTitle(notifyTitle)
                                .setContentText(notifyBody)
                                .setContentIntent(contentIntent)
                                .setSmallIcon(R.drawable.ic_notify_message)
                                .setAutoCancel(true)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
								.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
								.setColor(0x22667f)
								.setGroup("org.fox.ttirc")
                            	.setVibrate(priorityNotification ? new long[] {500, 1000}  : new long[0]);

							nmgr.notify(Constants.NOTIFY_UNREAD, builder.build());
					
						}
					} else if (findConnectionById(connectionId).active_nick.equals(message.sender)) {
						nmgr.cancel(Constants.NOTIFY_UNREAD);
					}
				}
			}
		} catch (Exception e) {
			Log.d(TAG, "pushMessage: error parsing message " + message.message);
			e.printStackTrace();
		}

		return message.id > m_lastId.get() ? message.id : m_lastId.get();
	}

	private void synthesizeTopicMessage(List<Message> list, int connectionId, String channel) {
		List<String> topic = m_channels.get(connectionId).get(channel).topic;
		
		if (topic != null && topic.size() == 3) {

			Message msg = new Message();
			
			msg.id = m_lastId.get();
			msg.channel = channel;
			msg.connection_id = connectionId;
			msg.message_type = Constants.MSGT_TOPIC;
			msg.incoming = true;
			msg.message = getString(R.string.message_topic, channel, topic.get(0));
			msg.date = new Date();
			msg.ts = m_dateFormatter.format(msg.date);
		
			list.add(msg);

			msg = new Message();

			msg.id = m_lastId.get();
			msg.channel = channel;
			msg.connection_id = connectionId;
			msg.message_type = Constants.MSGT_TOPIC;
			msg.incoming = true;
			msg.message = getString(R.string.message_topic_info, channel, topic.get(1), topic.get(2));
			msg.date = new Date();
			msg.ts = m_dateFormatter.format(msg.date);
			
			list.add(msg);

		}
	}

	private void setUnread(int connectionId, String channel, int unread) {
		HashMap<String, Integer> map = m_numUnreadMap.get(connectionId);

		if (map == null) {
			map = new HashMap<>();
			m_numUnreadMap.put(connectionId, map);
		}

		map.remove(channel);
		map.put(channel, unread);

	}

	private int getUnread(int connectionId, String channel) {
		try {
			return m_numUnreadMap.get(connectionId).get(channel);			
		} catch (Exception e) {
			// no entry
			return 0;
		}		
	}

	protected void updateServiceNotification() {
		Intent intent = new Intent(this, MainActivity.class);
		
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		Intent exitIntent = new Intent(this, MainActivity.class);
		exitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		exitIntent.setAction(CommonActivity.INTENT_ACTION_EXIT);

		Intent logoutIntent = new Intent(this, MainActivity.class);
		logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		logoutIntent.setAction(CommonActivity.INTENT_ACTION_LOGOUT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CommonActivity.NOTIFICATION_CHANNEL_SERVICE)
				.setContentTitle(getString(R.string.service_running))
                .setSmallIcon(R.drawable.ic_notify_service)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
				.addAction(R.drawable.ic_logout, getString(R.string.logout),
						PendingIntent.getActivity(this, 0, logoutIntent, PendingIntent.FLAG_UPDATE_CURRENT))
				.addAction(R.drawable.ic_logout, getString(R.string.menu_quit),
						PendingIntent.getActivity(this, 0, exitIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setPriority(NotificationCompat.PRIORITY_MIN)
				.setContentIntent(PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setColor(0x22667f)
                .setGroup("org.fox.ttirc");

        startForeground(Constants.NOTIFY_SERVICE_RUNNING, builder.build());
	}

	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

	@Override
	public void onCreate() {
		super.onCreate();		
		
		m_prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		
		m_serviceStarted = new Date();

		try {
			File file = new File(getCacheDir(), CONNSTATE_PERSIST_NAME + m_prefs.getString("login", ""));

			if (file.exists()) {
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));

				m_connections = (List<Connection>) ois.readObject();
				m_channels = (HashMap<Integer, HashMap<String, Channel>>) ois.readObject();

				ois.close();
			}

			notifyListeners();
		} catch (Exception e) {
			Log.d(TAG, "error deserializing " + CONNSTATE_PERSIST_NAME + m_prefs.getString("login", ""));
			e.printStackTrace();
		}

		try {
			File file = new File(getCacheDir(), EMOTICONS_PERSIST_NAME);

			if (file.exists()) {
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));

				m_emoticons = (HashMap<String, Emoticon>) ois.readObject();

				ois.close();
			}

			notifyListeners();

		} catch (Exception e) {
			Log.d(TAG, "error deserializing " + EMOTICONS_PERSIST_NAME);
			e.printStackTrace();
		}

		updateServiceNotification();

		m_updateThreadMonitor = new Timer("UpdateServiceMonitor");
		m_updateThreadMonitor.schedule(new TimerTask() {
			@Override
			public void run() {

				if (!m_runnableEnabled.get()) {
					m_updateThreadMonitor.cancel();
				}

				if (m_updateRunnableThread == null || m_updateRunnableThread.getState() == Thread.State.TERMINATED) {
					Log.d(TAG, "monitor: restarting runnable thread");

					m_updateRunnableThread = new Thread(m_updateRunnable);
					m_updateRunnableThread.start();
				}

				//Log.d(TAG, "monitor: " + m_updateRunnableThread.getState());

			}
		}, 1000L, 10000L);

		Log.i(TAG, "Service creating...");

		m_instance = this;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();

		Log.i(TAG, "Service destroying...");

		NotificationManager nmgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nmgr.cancel(Constants.NOTIFY_SERVICE_RUNNING);

		m_updateThreadMonitor.cancel();
		m_runnableEnabled.set(false);

		m_instance = null;
	}

}
