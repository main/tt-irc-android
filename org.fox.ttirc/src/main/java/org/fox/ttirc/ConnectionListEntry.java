package org.fox.ttirc;

import org.fox.ttirc.ConnectionsFragment.ConnectionListType;
import org.fox.ttirc.types.Channel;
import org.fox.ttirc.types.Connection;

class ConnectionListEntry {
	ConnectionListType m_type;
	Object m_contents;
	String m_title;
	String m_nativeTitle;
	int m_connectionId;
	int m_numUnread;
	boolean m_offline;

	ConnectionListEntry(Connection conn, int numUnread) {
		m_contents = conn;
		m_type = ConnectionListType.CONNECTION;
		m_title = conn.title;
		m_connectionId = conn.id;
		m_numUnread = numUnread;
		m_nativeTitle = "---";
		m_offline = false;
	}

	ConnectionListEntry(int connectionId, Channel chan,
						String title, int numUnread, boolean offline) {
		m_contents = chan;
		m_title = title;
		m_type = ConnectionListType.CHANNEL;
		m_connectionId = connectionId;
		m_numUnread = numUnread;
		m_nativeTitle = title;
		m_offline = offline;
	}
}

