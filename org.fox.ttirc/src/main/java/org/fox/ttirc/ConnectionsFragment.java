package org.fox.ttirc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.fox.ttirc.backend.UpdateService;
import org.fox.ttirc.backend.UpdateServiceApi;
import org.fox.ttirc.backend.UpdateServiceListener;
import org.fox.ttirc.net.HttpRequestTask;
import org.fox.ttirc.types.Channel;
import org.fox.ttirc.types.Connection;
import org.fox.ttirc.types.Constants;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import icepick.State;

public class ConnectionsFragment extends StateSavedFragment implements
		OnItemClickListener {
	private final String TAG = this.getClass().getSimpleName();
	private final String LISTENER_IDENT = "ConnectionsFragment";

	@State int m_activeConnection;
	@State String m_activeChannel;

	private UpdateServiceApi m_api;
	private List<ConnectionListEntry> m_connections = new ArrayList<ConnectionListEntry>();
	private ConnectionListAdapter m_adapter;
	protected SharedPreferences m_prefs;
	private MainActivity m_activity;
    private ListView m_list;

    public enum ConnectionListType {
		CONNECTION, CHANNEL
	}

    public ConnectionsFragment() {
		super();
	}

	public void initialize(int connectionId, String channel) {
		m_activeConnection = connectionId;
		m_activeChannel = channel;
	}
	
	@Override
	public void onResume() {
		super.onResume();

		m_activity.bindService(new Intent(m_activity, 
	            UpdateService.class), m_serviceConnection, 0);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuInfo) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo)menuInfo;
		ConnectionListEntry ce = (ConnectionListEntry) m_list.getItemAtPosition(info.position);
		
		if (ce.m_type == ConnectionListType.CONNECTION) {
			m_activity.getMenuInflater().inflate(R.menu.context_connection, menu);
			
			switch (m_api.getConnectionStatus(ce.m_connectionId)) {
			case Constants.CS_CONNECTED:
				menu.findItem(R.id.toggle_connection).setTitle(R.string.menu_disconnect);
				break;
			case Constants.CS_CONNECTING:
				menu.findItem(R.id.toggle_connection).setTitle(R.string.menu_connecting);
				break;
			default:
				menu.findItem(R.id.toggle_connection).setTitle(R.string.menu_connect);
			}
			
			
		} else {
			m_activity.getMenuInflater().inflate(R.menu.context_channel, menu);
		}
		
		menu.setHeaderTitle(ce.m_title);
		
		super.onCreateContextMenu(menu, v, menuInfo);		
	}

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_connections, container,
				false);

		m_list = view.findViewById(R.id.connections_list);

		if (m_activity.isSmallScreen()) {
			View layout = inflater.inflate(R.layout.drawer_header, m_list, false);
			m_list.addHeaderView(layout, null, false);

			TextView login = view.findViewById(R.id.drawer_header_login);
			TextView server = view.findViewById(R.id.drawer_header_server);

			login.setText(m_prefs.getString("login", ""));
			try {
				server.setText(new URL(m_prefs.getString("ttirc_url", "")).getHost());
			} catch (MalformedURLException e) {
				server.setText("");
			}

			View settings = view.findViewById(R.id.drawer_settings_btn);

			settings.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						Intent intent = new Intent(getActivity(),
								PreferencesActivity.class);
						startActivityForResult(intent, 0);
					} catch (Exception e) {

					}
				}
			});
		} else {
			View layout = inflater.inflate(R.layout.drawer_header_narrow, m_list, false);
			m_list.addHeaderView(layout, null, false);
		}

		View status = inflater.inflate(R.layout.connections_status_message, m_list, false);
		m_list.addHeaderView(status, null, false);

		m_list.setOnItemClickListener(this);

		m_adapter = new ConnectionListAdapter(m_activity,
				R.layout.connections_row,
				(ArrayList<ConnectionListEntry>) m_connections);

		m_list.setAdapter(this.m_adapter);

		registerForContextMenu(m_list);

		View loadingBar = view.findViewById(R.id.loading_bar);

		if (loadingBar != null)
			loadingBar.setVisibility(View.VISIBLE);

		return view;
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		final ConnectionListEntry ce = (ConnectionListEntry) m_list.getItemAtPosition(info.position);
		
		switch (item.getItemId()) {
		case R.id.toggle_connection:
			toggleActive(ce.m_connectionId);
			return true;
		case R.id.start_conversation:
			m_activity.queryUserDialog(ce.m_connectionId);			
			return true;
		case R.id.join_channel:
			m_activity.joinChannelDialog(ce.m_connectionId);			
			return true;
		case R.id.part_channel:
			m_api.send(ce.m_connectionId, ce.m_title, "/PART " + ce.m_title, null);
			return true;
		default:
			return super.onContextItemSelected(item);
		}		
	}

	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		m_prefs = PreferenceManager.getDefaultSharedPreferences(activity
				.getApplicationContext());
		
		m_activity = (MainActivity) activity;
	}

	void setActiveChannel(int connectionId, String channel) {
    	m_activeConnection = connectionId;
		m_activeChannel = channel;
		m_adapter.notifyDataSetChanged();
	}
	
	int getActiveConnection() {
		return m_activeConnection;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		if (m_api != null) {
			m_api.removeListener(LISTENER_IDENT);
		}

		m_activity.unbindService(m_serviceConnection);
	}

	private UpdateServiceListener m_updateListener = new UpdateServiceListener() {
		@Override
		public void handleUpdate() {
			//Log.i(TAG, "Received update request from the service");

			if (m_api == null || m_activity == null || getView() == null)
				return;

			final List<Connection> connections = m_api.getConnections();

			final HttpRequestTask.ErrorType lastError = m_api.getLastError();

			TextView status = getView().findViewById(R.id.service_status_message);

			if (status != null) {
				if (lastError != null && lastError != HttpRequestTask.ErrorType.NO_ERROR) {
					status.setVisibility(View.VISIBLE);
					status.setText(HttpRequestTask.getErrorMessage(lastError));
					getView().findViewById(R.id.loading_bar).setVisibility(View.VISIBLE);
					return;
				} else {
					status.setVisibility(View.GONE);
				}
			}

			if (connections == null || getView() == null)
				return;

			m_connections.clear();

			for (Connection conn : connections) {
				try {
					if (m_api == null)
						return;
					
					Bundle counters = m_api.getCounters(conn.id, "---");

					m_connections.add(new ConnectionListEntry(conn,
							counters.getInt("unread")));

					SortedMap<String, Channel> channels;
					
					try {
						channels = new TreeMap<>(m_api.getChannels(conn.id));
					} catch (NullPointerException e) {
						channels = null;
					}

					if (channels != null) {
						for (String channel : channels.keySet()) {
							Channel ichan = channels.get(channel);
							
							counters = m_api.getCounters(conn.id,
									channel);
							
							boolean isOffline = false;

							// 0 - C, 1 - P
							if (ichan.chan_type == 1) {
								isOffline = !conn.userhosts.containsKey(channel);
							}

							m_connections.add(new ConnectionListEntry(
									conn.id, ichan, channel, counters
											.getInt("unread"), isOffline));
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

            View loadingBar = getView().findViewById(R.id.loading_bar);

			if (loadingBar != null)
				loadingBar.setVisibility(View.INVISIBLE);

            m_adapter.notifyDataSetChanged();


		}
	};

	private ServiceConnection m_serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "Service connection established");
			m_api = (UpdateServiceApi)service;

			m_api.addListener(LISTENER_IDENT, m_updateListener);
			m_updateListener.handleUpdate();
		
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "Service connection closed");
			m_api = null;
		}
	};

	private class ConnectionListAdapter extends
			ArrayAdapter<ConnectionListEntry> {
		private ArrayList<ConnectionListEntry> items;

		static final int VIEW_CHANNEL = 0;
		static final int VIEW_CONNECTION = 1;
		static final int VIEW_CHANNEL_SELECTED = 2;
		static final int VIEW_CONNECTION_SELECTED = 3;
		static final int VIEW_CHANNEL_OFFLINE = 4;
		
		static final int VIEW_COUNT = VIEW_CHANNEL_OFFLINE + 1;

		ConnectionListAdapter(Context context, int textViewResourceId,
							  ArrayList<ConnectionListEntry> items) {
			super(context, textViewResourceId, items);
			this.items = items;
		}

		public int getViewTypeCount() {
			return VIEW_COUNT;
		}

		@Override
		public int getItemViewType(int position) {
			ConnectionListEntry ce = items.get(position);
			
			switch (ce.m_type) {
			case CHANNEL:
				return ce.m_connectionId == m_activeConnection && ce.m_title.equals(m_activeChannel) ? VIEW_CHANNEL_SELECTED : 
					ce.m_offline ? VIEW_CHANNEL_OFFLINE : VIEW_CHANNEL;
			default:
				return ce.m_connectionId == m_activeConnection && "---".equals(m_activeChannel) ? 
						VIEW_CONNECTION_SELECTED : VIEW_CONNECTION;
			}
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View v = convertView;

			ConnectionListEntry ce = items.get(position);

			if (v == null) {
				int layoutId;

				switch (getItemViewType(position)) {
				case VIEW_CONNECTION_SELECTED:
					layoutId = R.layout.connections_row_selected;
					break;
				case VIEW_CHANNEL_SELECTED:
					layoutId = R.layout.connections_row_chan_selected;
					break;
				case VIEW_CHANNEL:
					layoutId = R.layout.connections_row_chan;
					break;
				case VIEW_CHANNEL_OFFLINE:
					layoutId = R.layout.connections_row_chan_offline;
					break;
				default:
					layoutId = R.layout.connections_row;
					break;
				}

				LayoutInflater vi = (LayoutInflater) m_activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(layoutId, null);
			}

			TextView tt = v.findViewById(R.id.connections_row_name);

			if (tt != null) {
				tt.setText(ce.m_title);
			}

            /* ImageView img = (ImageView) v.findViewById(R.id.connections_row_status);

            if (img != null) {
                img.setImageResource(
                    m_prefs.getString("theme", CommonActivity.THEME_DEFAULT).equals(CommonActivity.THEME_HOLO) ?
                            R.drawable.ic_channel_light : R.drawable.ic_channel_dark);

            } */

			TextView tu = v
					.findViewById(R.id.connections_row_counter);

			if (tu != null) {
				tu.setText(String.valueOf(ce.m_numUnread));
				tu.setVisibility((ce.m_numUnread > 0) ? View.VISIBLE
						: View.INVISIBLE);
			}

			ImageView status = v
					.findViewById(R.id.connections_row_status);

			if (status != null && ce.m_type == ConnectionListType.CONNECTION) {
				Connection conn = (Connection) ce.m_contents;
				TypedValue tv = new TypedValue();

                TypedValue tvConnected = new TypedValue();
                m_activity.getTheme().resolveAttribute(R.attr.connectedColor, tvConnected, true);

				switch (conn.status) {
				case Constants.CS_DISCONNECTED:
				case Constants.CS_CONNECTING:
					m_activity.getTheme().resolveAttribute(R.attr.ic_offline, tv, true);
					status.setColorFilter(null);
					break;
				case Constants.CS_CONNECTED:
					m_activity.getTheme().resolveAttribute(R.attr.ic_online, tv, true);
					status.setColorFilter(tvConnected.data);
					break;
				}

                status.setImageResource(tv.resourceId);

			} else if (ce.m_type == ConnectionListType.CHANNEL) {

				TypedValue tv = new TypedValue();
				m_activity.getTheme().resolveAttribute(ce.m_title.indexOf("#") == 0 ? R.attr.ic_channel : R.attr.ic_account, tv, true);
				status.setImageResource(tv.resourceId);

			}

			return v;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> av, View view, int position, long id) {
		ListView list = getView().findViewById(
				R.id.connections_list);

		ConnectionListEntry ce = (ConnectionListEntry) list
				.getItemAtPosition(position);

    	m_activeConnection = ce.m_connectionId;
    	m_adapter.notifyDataSetChanged();

		m_activity.onChannelSelected(ce.m_connectionId, ce.m_nativeTitle, true);
	}

	void toggleActive(int connectionId) {
        Log.d(TAG, "toggleActive: " + connectionId);

		if (connectionId != 0 && m_api != null) {
			boolean enabled;

			switch (m_api.getConnectionStatus(connectionId)) {
			case Constants.CS_CONNECTED:
				enabled = false;
				break;
			default:
				enabled = true;
			}

            Log.d(TAG, "toggleActive: " + connectionId + " " + enabled);

            new ToggleConnectionTask(getActivity(), connectionId, m_api.getSessionId(), m_api.getCsrfToken()).execute(enabled);
		}
	}

    @SuppressLint("StaticFieldLeak")
	class ToggleConnectionTask extends HttpRequestTask {

        private final int m_connectionId;
		private final String m_csrfToken;

        ToggleConnectionTask(Context context, int connectionId, String sessionId, String csrfToken) {
            super(context, null, sessionId);

            m_requestUrl = m_prefs.getString("ttirc_url", "").trim() + "/backend.php?op=toggle-connection";

            m_connectionId = connectionId;
			m_csrfToken = csrfToken;

        }

        @SuppressWarnings("unchecked")
		void execute(final boolean enabled) {
            @SuppressWarnings("serial")
            HashMap<String, String> map = new HashMap<String, String>() {
                {
					put("csrf_token", m_csrfToken);
	                put("connection_id", String.valueOf(m_connectionId));
                    put("set_enabled", enabled ? "true" : "");
                }
            };

            super.execute(map);
        }

    }

	public void setActiveChannel(String channel) {
		m_activeChannel = channel;
		m_adapter.notifyDataSetChanged();
	}

}
