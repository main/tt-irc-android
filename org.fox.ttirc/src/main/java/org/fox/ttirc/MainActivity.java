package org.fox.ttirc;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.fox.ttirc.backend.UpdateService;
import org.fox.ttirc.backend.UpdateServiceApi;
import org.fox.ttirc.backend.UpdateServiceListener;
import org.fox.ttirc.net.HttpRequestTask;
import org.fox.ttirc.types.Constants;
import org.fox.ttirc.types.Emoticon;
import org.fox.ttirc.utils.ProgressRequestBody;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import icepick.State;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivity extends CommonActivity {
	private final String TAG = this.getClass().getSimpleName();
	private final String LISTENER_IDENT = "MainActivity";

	private static final String FRAG_CHANNEL = "channel";
	private static final String FRAG_CONNECTIONS = "connections";

	private SharedPreferences m_prefs;

	protected UpdateServiceApi m_api;
	protected Menu m_menu;

	private ActionBarDrawerToggle m_drawerToggle;
	@State
	String m_channel;

	public UpdateServiceListener m_updateListener = new UpdateServiceListener() {
		@Override
		public void handleUpdate() {
			//Log.i(TAG, "Received update request from the service");

			if (!m_api.loggedIn()) {
				logout(true);
				return;
			}

			invalidateOptionsMenu();
		}
	};

	private void initMenu() {
		if (m_api != null && m_menu != null) {

			ConnectionsFragment cof = (ConnectionsFragment) getSupportFragmentManager().findFragmentByTag(FRAG_CONNECTIONS);

			m_menu.setGroupVisible(R.id.menu_group_connections, cof != null);

			MenuItem toggleMenuItem = m_menu.findItem(R.id.toggle_connection);

			ChannelView chf = (ChannelView) getSupportFragmentManager().findFragmentByTag(FRAG_CHANNEL);

			m_menu.setGroupVisible(R.id.menu_group_connection, chf != null && "---".equals(chf.getChannel()));
			m_menu.setGroupVisible(R.id.menu_group_channel, chf != null && !"---".equals(chf.getChannel()));

			m_menu.findItem(R.id.channel_set_topic).setVisible(chf != null && chf.getChannel() != null && chf.getChannel().indexOf('#') == 0);
			m_menu.findItem(R.id.channel_emoticons).setVisible(m_prefs.getBoolean("enable_emoticons", true) && chf != null && chf.getChannel() != null);

			if (isSmallScreen())
				m_menu.findItem(R.id.open_preferences).setVisible(false);

			if (chf != null) {
				if ((!isSmallScreen() && !isPortrait()) || (chf.getChannel().charAt(0) != '#' && chf.getChannel().charAt(0) != '&')) {
					m_menu.findItem(R.id.channel_toggle_nicks).setVisible(false);
				}

				switch (m_api.getConnectionStatus(chf.getConnectionId())) {
					case Constants.CS_CONNECTED:
						toggleMenuItem.setTitle(R.string.menu_disconnect);
						//toggleMenuItem.setIcon(R.drawable.ic_server_network_off);
						break;
					case Constants.CS_CONNECTING:
						toggleMenuItem.setTitle(R.string.menu_connecting);
						//toggleMenuItem.setIcon(R.drawable.ic_server_network_off);
						break;
					default:
						toggleMenuItem.setTitle(R.string.menu_connect);
						//toggleMenuItem.setIcon(R.drawable.ic_server_network);
						break;
				}
			}
		}
	}

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");

		m_prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		setSmallScreen(findViewById(R.id.sw600dp_anchor) == null);

		SharedPreferences.Editor editor = m_prefs.edit();
		editor.putBoolean("small_screen_mode", isSmallScreen());
		editor.apply();

		//setProgressBarIndeterminateVisibility(false);

		if (savedInstanceState == null) {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.replace(R.id.connections_container, new ConnectionsFragment(), FRAG_CONNECTIONS);
			ft.commit();
		}

		DrawerLayout drawerLayout = findViewById(R.id.main_drawer);

		if (drawerLayout != null) {
			if (m_channel != null) {
				setTitle(m_channel);
			}

			m_drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
				@Override
				public void onDrawerOpened(View drawerView) {
					invalidateOptionsMenu();
				}

				@Override
				public void onDrawerClosed(View drawerView) {
					invalidateOptionsMenu();
				}
			};

			drawerLayout.setDrawerListener(m_drawerToggle);

			m_drawerToggle.setDrawerIndicatorEnabled(true);

			if (savedInstanceState == null) {
				drawerLayout.openDrawer(Gravity.LEFT);
			}

			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setHomeButtonEnabled(true);
		}

	}

	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		if (m_drawerToggle != null) m_drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (m_drawerToggle != null) m_drawerToggle.onConfigurationChanged(newConfig);
	}

	protected void logout(boolean autoLogin) {

		String ttircUrl = m_prefs.getString("ttirc_url", "");

		try {
			HttpRequestTask task = new HttpRequestTask(getApplicationContext(),
					ttircUrl.trim() + "/backend.php?op=logout", "");
			task.execute(new HashMap<String, String>());
		} catch (Exception e) {
			e.printStackTrace();
		}

		Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("autoLogin", autoLogin);
		startActivityForResult(intent, 0);
		finish();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		Log.d(TAG, "onNewIntent: " + intent);

		if (CommonActivity.INTENT_ACTION_EXIT.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(MainActivity.this, UpdateService.class);
            stopService(serviceIntent);

			finish();
			return;
		} else if (CommonActivity.INTENT_ACTION_LOGOUT.equals(intent.getAction())) {
			logout(false);
			return;
		}

		Bundle extras = intent.getExtras();

		if (extras != null) {
			String channel = extras.getString("channel");
			int connectionId = extras.getInt("connectionId");

			if (channel != null && connectionId > 0) {
				Log.d(TAG, "got: " + channel + " " + connectionId);

				onChannelSelected(connectionId, channel, true);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		bindService(new Intent(MainActivity.this,
				UpdateService.class), m_serviceConnection, 0);

		clearPendingNotifications();

		if (!isServiceRunning())
				logout(true);

		Log.d(TAG, "onResume");
	}

	public boolean isServiceRunning() {
		return UpdateService.isInstanceCreated();
	}

	public void clearPendingNotifications() {
		NotificationManager nmgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nmgr.cancel(Constants.NOTIFY_HIGHLIGHT);
		nmgr.cancel(Constants.NOTIFY_UNREAD);
		nmgr.cancel(Constants.NOTIFY_PRIVATE);
	}

	@Override
	public void onPause() {
		super.onPause();

		if (m_api != null) {
			m_api.removeListener(LISTENER_IDENT);
		}

		unbindService(m_serviceConnection);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		try {
			unbindService(m_serviceConnection);
		} catch (IllegalArgumentException e) {
			// not bound yet
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);

		m_menu = menu;

		initMenu();

		return true;
	}

	protected void joinChannelDialog(final int connectionId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.menu_channel_join));
		final EditText chanEdit = new EditText(this);
		builder.setView(chanEdit);

		builder.setPositiveButton(getString(R.string.dialog_join), new Dialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				String chan = chanEdit.getText().toString().trim();

				m_api.send(connectionId, "---", "/JOIN " + chan, null);
			}
		});

		builder.setNegativeButton(getString(R.string.dialog_cancel), new Dialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				//
			}
		});

		AlertDialog dlg = builder.create();
		dlg.show();
	}

	protected void queryUserDialog(final int connectionId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.nick_start_conversation));
		final EditText chanEdit = new EditText(this);
		builder.setView(chanEdit);

		builder.setPositiveButton(getString(R.string.nick_start_conversation), new Dialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				String chan = chanEdit.getText().toString().trim();

				m_api.send(connectionId, "---", "/QUERY " + chan, null);
			}
		});

		builder.setNegativeButton(getString(R.string.dialog_cancel), new Dialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				//
			}
		});

		AlertDialog dlg = builder.create();
		dlg.show();
	}

	@Override
	public boolean onOptionsItemSelected(@NotNull MenuItem item) {
		Intent intent;
		final ChannelView cf = (ChannelView) getSupportFragmentManager().findFragmentByTag(FRAG_CHANNEL);

		if (m_drawerToggle != null && m_drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {
			case android.R.id.home:
				DrawerLayout drawerLayout = findViewById(R.id.main_drawer);

				if (drawerLayout != null) {
					if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
						drawerLayout.closeDrawers();
					} else {
						drawerLayout.openDrawer(GravityCompat.START);
					}
				}

				return true;
			case R.id.open_preferences:
			case R.id.preferences:
				intent = new Intent(this,
						PreferencesActivity.class);

				startActivityForResult(intent, 0);
				return true;
			case R.id.logout:
				logout(false);
				return true;
			case R.id.toggle_connection:
				ConnectionsFragment cof = (ConnectionsFragment) getSupportFragmentManager()
						.findFragmentByTag(FRAG_CONNECTIONS);
				if (cof != null) {
					cof.toggleActive(cof.getActiveConnection());
				}
				return true;
			case R.id.channel_emoticons:
				if (true) {
					ArrayList<Emoticon> emoticons = new ArrayList<>(m_api.getEmoticons(false).values());
					ArrayList<Emoticon> favorites = new ArrayList<>(m_api.getEmoticons(true).values());

					new EmoticonSearchDialog(MainActivity.this, "",
							"Search emoticons...", null, emoticons, favorites,
							new SearchResultListener<Emoticon>() {
								@Override
								public void onSelected(BaseSearchDialogCompat dialog,
													   Emoticon item, int position) {

									ChannelView chf = (ChannelView) getSupportFragmentManager().findFragmentByTag(FRAG_CHANNEL);

									if (chf != null) {
										chf.appendText(item.name);
									}

									dialog.dismiss();
								}
							}, R.style.DialogTheme_Compat_Light).show();

				}
				return true;
			case R.id.channel_toggle_nicks:
				DrawerLayout drawer = findViewById(R.id.channel_drawer);

				if (drawer != null) {
					drawer.openDrawer(GravityCompat.END);
				}

				return true;
			case R.id.join_channel:
				if (cf != null && m_api != null) {
					joinChannelDialog(cf.getConnectionId());
				}

				return true;
			case R.id.upload_file:
				if (cf != null && m_api != null) {
					Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(pickIntent, REQUEST_IMAGE_PICKER);
				}

				return true;
			case R.id.channel_get_history:
				if (cf != null && m_api != null) {
					m_api.requestHistory(cf.getConnectionId(), cf.getChannel(), cf.getMessagesCount());
				}
				return true;
			case R.id.channel_set_topic:
				if (cf != null && m_api != null) {

					String topic = m_api.getTopic(cf.getConnectionId(), cf.getChannel());

					AlertDialog.Builder topicBuilder = new AlertDialog.Builder(this);
					topicBuilder.setTitle(getString(R.string.channel_new_topic, cf.getChannel()));
					final EditText topicEdit = new EditText(this);
					topicEdit.setText(topic);
					topicBuilder.setView(topicEdit);

					topicBuilder.setPositiveButton(getString(R.string.dialog_set_topic), new Dialog.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							String newTopic = topicEdit.getText().toString().trim();

							m_api.send(cf.getConnectionId(), cf.getChannel(), "/TOPIC " + newTopic, null);
						}
					});

					topicBuilder.setNegativeButton(getString(R.string.dialog_cancel), new Dialog.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							//
						}
					});

					AlertDialog topicDlg = topicBuilder.create();
					topicDlg.show();
				}

				return true;
			case R.id.part_channel:
				if (m_api != null && cf != null) {
					m_api.send(cf.getConnectionId(), cf.getChannel(), "/PART " + cf.getChannel(), null);
					finish();
				}
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public ServiceConnection m_serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "Service connection established");
			m_api = (UpdateServiceApi) service;

			m_api.addListener(LISTENER_IDENT, m_updateListener);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "Service connection closed");
			m_api = null;
		}
	};

	public void onChannelSelected(final int connectionId, final String channel, boolean open) {
		clearPendingNotifications();

		if (!isServiceRunning()) {
			logout(true);
			return;
		}

		if (open) {
			final DrawerLayout drawerLayout = findViewById(R.id.main_drawer);

			if (drawerLayout != null) {
				drawerLayout.closeDrawers();
			}

			ChannelFragment cf = (ChannelFragment) getSupportFragmentManager().findFragmentByTag(FRAG_CHANNEL);

			if (m_api != null && cf != null && cf.getChannel().equals(channel) && cf.getConnectionId() == connectionId) {
				m_api.setActiveChannel(connectionId, channel); // reset unread count
				return; // channel already open
			}

			m_channel = channel;

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

					ChannelFragment cf = new ChannelFragment();
					cf.initialize(connectionId, channel);

					ft.replace(R.id.channel_container, cf, FRAG_CHANNEL);

					if (drawerLayout != null) {
						setTitle(channel);
					} else {
						ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
					}

					ft.commit();
				}
			}, drawerLayout != null ? 200 : 0);

		}

		ConnectionsFragment cf = (ConnectionsFragment) getSupportFragmentManager().findFragmentByTag(FRAG_CONNECTIONS);

		if (cf != null) {
			cf.setActiveChannel(connectionId, channel);
		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (m_api != null)
					m_api.setActiveChannel(connectionId, channel);
			}
		}, 100);

	}

	public void showProgressBar(boolean show) {
		View progressBar = findViewById(R.id.main_progress_bar);
		if (progressBar != null) {
			progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
		}
	}

	public Bitmap loadEmoticonBitmap(Emoticon emot) {
		File imageFile = new File(emot.localfile);

		if (imageFile.exists() && emot.height > 0) {
			try {
				BitmapFactory.Options options = new BitmapFactory.Options();
				DisplayMetrics metrics = getResources().getDisplayMetrics();

				options.inJustDecodeBounds = true;
				BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);

				options.inJustDecodeBounds = false;
				Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);

				if (bitmap != null) {
					int scaledWidth = (int) (options.outWidth * (emot.height / (float) options.outHeight));

					return Bitmap.createScaledBitmap(bitmap,
							(int) (scaledWidth * metrics.density),
							(int) (emot.height * metrics.density), true);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static String sha1(String clearString) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
			messageDigest.update(clearString.getBytes(StandardCharsets.UTF_8));

			StringBuilder buffer = new StringBuilder();
			for (byte b : messageDigest.digest()) {
				buffer.append(String.format(Locale.getDefault(), "%02x", b));
			}
			return buffer.toString();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK && requestCode == REQUEST_IMAGE_PICKER) {
			final Uri selectedMediaUri = data.getData();

			Log.d(TAG, "[media picker] got URI: " + selectedMediaUri);

			try {
				UploadFileTask uploadTask = new UploadFileTask();
				uploadTask.attachStreamUri(selectedMediaUri);

				uploadTask.execute(m_prefs.getString("ttirc_url", "").trim() + "/backend.php",
						m_api.getSessionId(),
						m_api.getCsrfToken());

			} catch (Exception e) {
				toast(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	@SuppressLint("StaticFieldLeak")
	private class UploadFileTask extends AsyncTask<String, Integer, JsonElement> {

		private Uri m_streamUri;
		private int m_totalSize = 1;
		private ProgressDialog m_progressDialog = new ProgressDialog(MainActivity.this);
		private String m_lastError = null;

		void attachStreamUri(Uri uri) {
			m_streamUri = uri;
		}

		@Override
		protected void onPreExecute() {
			m_progressDialog.setMessage(getString(R.string.upload_progress_message));
			m_progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			m_progressDialog.setCancelable(false);
			m_progressDialog.show();
		}

		@Override
		protected JsonElement doInBackground(String... args) {
			String requestUrl = args[0];
			final String sid = args[1];
			String csrf_token = args[2];

			try {
				if (requestUrl != null) {

					OkHttpClient client = new OkHttpClient();

					InputStream inputStream = getContentResolver().openInputStream(m_streamUri);
					m_totalSize = inputStream.available();

					String fileName = "upload.jpg";

					if (m_streamUri.getPath() != null)
						fileName = new File(m_streamUri.getPath()).getName();

					ProgressRequestBody progressRequestBody = new ProgressRequestBody(
							inputStream,
							"image/jpeg", new ProgressRequestBody.ProgressListener() {
						@Override
						public void transferred(long num) {
							Log.d(TAG, "uploaded " + num + " of " + m_totalSize);
							publishProgress((int) ((num / (float) m_totalSize) * 100));
						}
					});

					RequestBody requestBody = new MultipartBody.Builder()
						.setType(MultipartBody.FORM)
						.addFormDataPart("op", "uploadandpost")
						.addFormDataPart("csrf_token", csrf_token)
						.addFormDataPart("file", fileName, progressRequestBody)
						.build();

					Request request = new Request.Builder()
						.addHeader("Cookie", "ttirc_sid2=" + sid)
						.url(requestUrl)
						.post(requestBody)
						.build();

					Response response = client.newCall(request).execute();

					if (!response.isSuccessful())
						throw new IOException("Unexpected HTTP code: " + response.code());

					ResponseBody responseBody = response.body();

					if (responseBody != null) {
						JsonParser parser = new JsonParser();

						return parser.parse(responseBody.string());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				m_lastError = e.getMessage();
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			m_progressDialog.setProgress(progress[0]);
		}

		@Override
		protected void onPostExecute(JsonElement result) {

			if (m_progressDialog.isShowing())
				m_progressDialog.dismiss();

			if (result != null && result.isJsonObject()) {
				Log.d(TAG, "upload result=" + result.toString());

				JsonObject resultObj = result.getAsJsonObject();
				JsonElement status = resultObj.get("status");
				JsonElement reason = resultObj.get("reason");
				JsonElement uploadUrl = resultObj.get("upload_url");

				if (uploadUrl != null) {
					ChannelView cf = (ChannelView) getSupportFragmentManager().findFragmentByTag(FRAG_CHANNEL);

					if (cf != null) {
						cf.appendText(uploadUrl.getAsString());
					}

				} else {
					if (status != null && reason != null) {
						toast(reason.toString());
					} else if (m_lastError != null) {
						toast(m_lastError);
					} else {
						toast(R.string.error_upload_failed);
					}
				}

			} else if (m_lastError != null) {
				toast(m_lastError);
			} else {
				toast(R.string.error_upload_failed);
			}

		}
	}

}