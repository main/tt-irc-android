package org.fox.ttirc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.fox.ttirc.backend.UpdateService;
import org.fox.ttirc.backend.UpdateServiceApi;
import org.fox.ttirc.backend.UpdateServiceListener;
import org.fox.ttirc.net.HttpRequestTask;
import org.fox.ttirc.types.Constants;
import org.fox.ttirc.types.Emoticon;
import org.fox.ttirc.types.Message;
import org.fox.ttirc.utils.CustomLinkify;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import icepick.State;

public class ChannelFragment extends StateSavedFragment implements OnEditorActionListener, ChannelView {
	private final String TAG = this.getClass().getSimpleName();
	private final static int MAX_MESSAGES_IN_BUFFER = 1024;

	private String m_listenerId = "ChannelFragment";

	private UpdateServiceApi m_api;
	@State int m_connectionId;
	@State String m_channelName = "";
	private String m_selfNick;
	private NickListAdapter m_nickAdapter;
	private MessageListAdapter m_messageAdapter;	
	private List<String> m_nickList = new ArrayList<String>();
	private List<Message> m_messages = new ArrayList<Message>();
	@State String m_topic = "";
	private SharedPreferences m_prefs;
	private MainActivity m_activity;
	private HashMap<String, Bitmap> m_emoticonBitmaps = new HashMap<>();
	private HashMap<String, Emoticon> m_emoticons = new HashMap<>();
	private ConcurrentHashMap<String, JsonObject> m_urlMetadata = new ConcurrentHashMap<>();
	private int m_lastPos;

	private static int[] ColorMapLight = { 0xff00aaaa, 0xff000000, 0xff0000CC,
			0xffCC00CC, 0xff606060, 0xff00cc00, 0xff30a030, 0xff8040cc,
			0xff000080, 0xff707000, 0xff800080, 0xffcc3030, 0xff707070,
			0xff008080, 0xffaaaa00 };

	private static int[] ColorMapDark = { 0xff40CCCC, 0xffcccccc, 0xff4040CC,
			0xffCC40CC, 0xff909090, 0xff40FF40, 0xff40CC40, 0xff8040ff,
			0xff9090f0, 0xff808040, 0xffcc80cc, 0xffFF4040, 0xff909090,
			0xff40a0a0, 0xffCCCC40 };
	private ListView m_messagesList;

	public enum MessageEventType { TOPIC, MODE, KICK, PART, JOIN, QUIT, DISCONNECT,
		REQUEST_CONNECTION, CONNECTING, PING_REPLY, NOTICE, CTCP, CTCP_REPLY, PING, CONNECT,
		UNKNOWN_CMD, NICK, ERROR, SERVER_PONG, UNAWAY }

    public ChannelFragment() {
		super();
	}

	void initialize(int connectionId, String channelName) {
		m_connectionId = connectionId;
		m_channelName = channelName;
		
		m_listenerId += channelName + ":" + connectionId;
	}

	@Override
	public int getConnectionId() {
		return m_connectionId;
	}

	@Override
	public String getChannel() {
		return m_channelName;
	}
	
	@Override
	public void onResume() {
		super.onResume();

		m_activity.bindService(new Intent(m_activity,
	            UpdateService.class), m_serviceConnection, 0);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		
		switch (item.getItemId()) {
		case R.id.nick_start_conversation:
			String nick = getNickAtPosition(info.position);
				
			if (nick != null) {				
				if (nick.charAt(0) == '+' || nick.charAt(0) == '@') nick = nick.substring(1);
			
				final String fNick = nick;
				
				m_activity.showProgressBar(true);
				
				m_api.send(m_connectionId, m_channelName, "/QUERY " + nick, new UpdateServiceApi.OnSendCompleted() {
					@Override
					public void onSendCompleted(JsonElement result) {
						m_activity.showProgressBar(false);
						m_activity.onChannelSelected(m_connectionId, fNick, true);
					}
				});
			}
			return true;
		default:
			return super.onContextItemSelected(item);
		}		
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		m_activity = (MainActivity) activity;
		m_prefs = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
    }
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	    ContextMenuInfo menuInfo) {
		
		m_activity.getMenuInflater().inflate(R.menu.context_nick, menu);
		
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)menuInfo;
		String nick = m_nickAdapter.getItem(info.position);
		
		if (nick.charAt(0) == '+' || nick.charAt(0) == '@')
			nick = nick.substring(1);
		
		menu.setHeaderTitle(nick);
		
		super.onCreateContextMenu(menu, v, menuInfo);		
	}
	
	private String getNickAtPosition(int position) {
		return m_nickAdapter.getItem(position);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    	

		final View view = inflater.inflate(R.layout.fragment_channel, container, false);

        final EditText input = view.findViewById(R.id.channel_input_text);
		input.setOnEditorActionListener(ChannelFragment.this);

		ImageView sendButton = view.findViewById(R.id.send_button);
		
		if (sendButton != null) {
			sendButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {

					messagesToBottom();

					String inputStr = input.getText().toString();

					if (input.length() > 0) {
						m_activity.showProgressBar(true);

						m_api.send(m_connectionId, m_channelName, inputStr, new UpdateServiceApi.OnSendCompleted() {
							@Override
							public void onSendCompleted(JsonElement result) {
								m_activity.showProgressBar(false);
							}
						});
						
						input.setText("");
					}
				}
			});
		}

        /*if (m_channelName.charAt(0) != '#' || (m_activity.isPortrait() && m_activity.isSmallScreen())) {
            view.findViewById(R.id.channel_nick_list).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.channel_nick_list).setVisibility(m_activity.getShowNicks() ? View.VISIBLE : View.GONE);
        }*/

        new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {

				if (isAdded()) {

					ListView nickList = view.findViewById(R.id.channel_nick_list);
					m_nickAdapter = new NickListAdapter(m_activity, R.layout.nicklist_row, (ArrayList<String>)m_nickList);
					nickList.setAdapter(m_nickAdapter);
					registerForContextMenu(nickList);

					nickList.setOnItemClickListener(new OnItemClickListener() {
	
						@Override
						public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {
							
							String nick = m_nickAdapter.getItem(position);
	
							if (nick.charAt(0) == '+' || nick.charAt(0) == '@')
								nick = nick.substring(1);
							
							EditText input = getView().findViewById(R.id.channel_input_text);
							
							if (input != null) {
								nick = nick + ": "; // TODO customize completion character
								
								String text = input.getText().toString();
	
								if (text.indexOf(nick) != 0) {
									input.setText(nick + text);
									input.setSelection(input.getText().length());
								}
							}				
						}
						
					});

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isAdded()) {
                                m_messagesList = getView().findViewById(R.id.channel_message_list);
                                m_messageAdapter = new MessageListAdapter(m_activity, R.layout.messagelist_row_system, (ArrayList<Message>)m_messages);
                                m_messagesList.setAdapter(m_messageAdapter);

                                m_messagesList.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
                                m_messagesList.setStackFromBottom(true);

                                /* m_messagesList.setOnScrollListener(new AbsListView.OnScrollListener() {
									@Override
									public void onScrollStateChanged(AbsListView absListView, int scrollState) {
									}

									@Override
									public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
										if (totalItemCount > 0 && firstVisibleItem == 0) {
											m_api.requestHistory(m_connectionId, m_channelName, m_messages.size());
										}
									}
								}); */

								if (m_channelName.charAt(0) != '#' && m_channelName.charAt(0) != '&') {
                                    DrawerLayout drawer = view.findViewById(R.id.channel_drawer);

                                    if (drawer != null)
									    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                                }
                            }
                        }
                    }, 100);

                }
			}
			
		}, 100);

		return view;    	
	}

    public void appendText(String text) {
        EditText input = getView().findViewById(R.id.channel_input_text);

        if (input.getText().length() > 0) {
            input.append(" ");
        }

        input.append(text + " ");
    }
	
	@Override
	public void onPause() {
		super.onPause();

		if (m_api != null) {
			m_api.removeListener(m_listenerId);
		}

		m_activity.unbindService(m_serviceConnection);
	}
	
	private UpdateServiceListener m_updateListener = new UpdateServiceListener() {
		@Override
		public void handleUpdate() {
			//Log.i(TAG, "Received update request from the service");

			if (m_api == null || m_activity == null || !isAdded() || getView() == null)
				return;

			String topicStr;

			final List<String> nicks = m_api.getNicks(m_connectionId, m_channelName);

			//Log.d(TAG, "requesting messages for " + m_channelName + " since pos" + m_lastPos);

			final List<Message> messages = m_api.getMessages(m_connectionId, m_channelName, m_lastPos);

			if (m_emoticons.size() == 0)
				m_emoticons.putAll(m_api.getEmoticons(false));

			topicStr = m_api.getTopic(m_connectionId, m_channelName);

			m_selfNick = m_api.getSelfNick(m_connectionId);

			if (nicks != null) {
				if (m_nickList.size() != nicks.size()) {
					m_nickList.clear();
					m_nickList.addAll(nicks);

					if (m_nickAdapter != null) {
						m_nickAdapter.notifyDataSetChanged();
					}
				}
			}

			final HttpRequestTask.ErrorType lastError = m_api.getLastError();

			TextView status = getView().findViewById(R.id.service_status_message);

			if (status != null) {
				if (lastError != null && lastError != HttpRequestTask.ErrorType.NO_ERROR) {
					status.setVisibility(View.VISIBLE);
					status.setText(HttpRequestTask.getErrorMessage(lastError));
				} else {
					status.setVisibility(View.GONE);
				}
			}

			if (messages != null && messages.size() > 0) {
				for (Message msg : messages) {
					m_messages.add(insertPos(msg), msg);
				}
				m_lastPos += messages.size();

				// when expiring we need to keep in sync with the service buffer
				// which is ordered differently (not sorted by ID)
                while (m_messages.size() > MAX_MESSAGES_IN_BUFFER + 100) {
					List<Message> msgs = m_messages.subList(0, 100);

                    m_lastPos -= 100;

                    m_api.expireMessages(m_connectionId, m_channelName, msgs);
                    msgs.clear();
                }

				if (m_messageAdapter != null) {
					m_messageAdapter.notifyDataSetChanged();
				}
			}

			TextView nick = getView().findViewById(R.id.channel_nick);
			if (nick != null)
				nick.setText(m_selfNick);

			TextView topic = getView().findViewById(R.id.channel_topic_text);
			if (topic != null && topicStr != null) {

				SpannableString span = new SpannableString(topicStr);

				span = rewriteEmoticons(span);

				topic.setText(span);
			}

			getView().findViewById(R.id.channel_loading_bar).setVisibility(View.GONE);
		}

		int insertPos(Message message) {
			for (int i = 0; i < m_messages.size(); i++) {
				Message msg = m_messages.get(i);
				Message prev = i > 0 ? m_messages.get(i-1) : null;

				if (message.id < msg.id && (prev == null || message.id > prev.id)) {
					return i;
				}
			}

			return m_messages.size();
		}
	};

	private ServiceConnection m_serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "Service connection established");
			m_api = (UpdateServiceApi)service;
			
			m_api.addListener(m_listenerId, m_updateListener);
			m_updateListener.handleUpdate();
		
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "Service connection closed");
			m_api = null;
		}
	}; 

	private class NickListAdapter extends ArrayAdapter<String> {
		private ArrayList<String> items;

		NickListAdapter(Context context, int textViewResourceId, ArrayList<String> items) {
			super(context, textViewResourceId, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View v = convertView;

			String nick = items.get(position);

			if (v == null) {
				LayoutInflater vi = (LayoutInflater)m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.nicklist_row, null);
			}

			TextView nt = v.findViewById(R.id.nicklist_row_nick);
			ImageView icon = v.findViewById(R.id.nicklist_row_icon);

			if (icon != null && nt != null) {
				char prefix = nick.charAt(0);

				switch (prefix) {
				case '@':
					icon.setImageResource(R.drawable.user_op);
					nt.setText(nick.substring(1));
					break;            	 
				case '+':
					icon.setImageResource(R.drawable.user_voice);
					nt.setText(nick.substring(1));
					break; 
				default:
					icon.setImageResource(R.drawable.user_normal);
					nt.setText(nick);
					break;
				}        	 

				//        	 icon.setBackgroundResource(R.drawable.user_normal);
				//        	 icon.setAdjustViewBounds(true);        	 
			}           

			return v;
		}
	}

    private class MessageListHolder {
        public TextView message;
		ImageView imageThumbnail;
		View metadataContainer;
		ImageView metadataThumbnail;
		TextView metadataSummary;
		TextView metadataTitle;
	}

	private class MessageListAdapter extends ArrayAdapter<Message> {
		private final int m_screenHeight;
		private ArrayList<Message> items;

		static final int VIEW_PRIVMSG = 0;
		static final int VIEW_ACTION = 1;
		static final int VIEW_NOTICE = 2;
		static final int VIEW_SYSTEM = 3;
		
		static final int VIEW_COUNT = VIEW_SYSTEM + 1;

		MessageListAdapter(Context context, int textViewResourceId, ArrayList<Message> items) {
			super(context, textViewResourceId, items);
			this.items = items;

			Display display = m_activity.getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			m_screenHeight = size.y;
		}

		public int getViewTypeCount() {
			return VIEW_COUNT;
		}

		@Override
		public int getItemViewType(int position) {
			Message msg = items.get(position);

			switch (msg.message_type) {
			case Constants.MSGT_PRIVMSG:
			case Constants.MSGT_PRIVATE_PRIVMSG:
				return VIEW_PRIVMSG;
			case Constants.MSGT_NOTICE:
				return VIEW_NOTICE;
			case Constants.MSGT_ACTION:
				return VIEW_ACTION;
			default:
				return VIEW_SYSTEM;
			}
		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View v = convertView;

			final Message msg = items.get(position);
			final Bundle fmt = formatMessage(msg);
            final MessageListHolder holder;

            if (v == null) {
				LayoutInflater vi = (LayoutInflater)m_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				int layoutId; 

				switch (getItemViewType(position)) {
				case VIEW_PRIVMSG:
					layoutId = R.layout.messagelist_row_privmsg;
					break;
				case VIEW_ACTION:
					layoutId = R.layout.messagelist_row_action;
					break;
				case VIEW_NOTICE:
					layoutId = R.layout.messagelist_row_notice;
					break;            		
				default:
					layoutId = R.layout.messagelist_row_system;
				}

				v = vi.inflate(layoutId, null);

                holder = new MessageListHolder();

                holder.message = v.findViewById(R.id.message);
                holder.imageThumbnail = v.findViewById(R.id.image_thumbnail);

				holder.metadataContainer = v.findViewById(R.id.metadata_container);
                holder.metadataThumbnail = v.findViewById(R.id.metadata_image);
				holder.metadataTitle = v.findViewById(R.id.metadata_title);
				holder.metadataSummary = v.findViewById(R.id.metadata_summary);

                v.setTag(holder);
			} else {
                holder = (MessageListHolder) v.getTag();
            }

			//TextView message = (TextView) v.findViewById(R.id.message);
		
			if (holder.message != null) {
				
				SpannableString span; 

				String ts = msg.ts.substring(11);
				
				TypedValue tv = new TypedValue();
			    m_activity.getTheme().resolveAttribute(R.attr.messageTsColor, tv, true);
			    int tsColor = tv.data;

				switch (msg.message_type) {
				case Constants.MSGT_PRIVMSG:
				case Constants.MSGT_PRIVATE_PRIVMSG:
				case Constants.MSGT_NOTICE:
					String nick = fmt.getString("sender");
				    String ownNick = "";
				    
					ownNick = m_api.getSelfNick(m_connectionId);
					
					int nickColor = 0xffffffff;
					
					if (ownNick.equals(nick)) {
					    m_activity.getTheme().resolveAttribute(R.attr.messageOwnNickColor, tv, true);
					    nickColor = tv.data;
					} else {					
						try {
							nickColor = !m_activity.isUiNightMode() ? ColorMapLight[msg.sender_color] :
								ColorMapDark[msg.sender_color];
						} catch (ArrayIndexOutOfBoundsException e) {
							// wtf, backend!
						    m_activity.getTheme().resolveAttribute(R.attr.messageNickColor, tv, true);
						    nickColor = tv.data;
						}
					}

				    m_activity.getTheme().resolveAttribute(R.attr.messageBracketColor, tv, true);
				    int bracketColor = tv.data;

					if (msg.message_type == Constants.MSGT_NOTICE) {
						span = new SpannableString(ts + " –" + nick + "– " + fmt.getString("message"));
					} else {
						span = new SpannableString(ts + " <" + nick + "> " + fmt.getString("message"));
					}
				    //CharSequence message = fmt.getString("message"); 
				    
				    //Log.d(TAG, String.format("color=%x [%d]", ColorMap[msg.sender_color], msg.sender_color));
				    
					span.setSpan(new ForegroundColorSpan(tsColor), 0, ts.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					span.setSpan(new ForegroundColorSpan(bracketColor), ts.length()+1, ts.length()+2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					span.setSpan(new ForegroundColorSpan(nickColor), ts.length()+2, ts.length()+2+nick.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					span.setSpan(new ForegroundColorSpan(bracketColor), ts.length()+2+nick.length(), ts.length()+2+nick.length()+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

					span = rewriteEmoticons(span);

                    holder.message.setText(span);

					if (holder.imageThumbnail != null) {

						holder.imageThumbnail.setVisibility(View.GONE);
						holder.metadataContainer.setVisibility(View.GONE);
						Glide.clear(holder.imageThumbnail);

						Pattern pattern = Pattern.compile("https?://[^ ]+");
						Matcher matcher = pattern.matcher(span);

						if (matcher.find()) {
							final String url = matcher.group();
							final String ttircUrl = m_prefs.getString("ttirc_url", "");

							if (m_urlMetadata.containsKey(url)) {
								//Log.d(TAG, "using cached metadata:" + url);

								applyUrlMetadata(url, holder);
							} else {
								//Log.d(TAG, "requesting metadata for:" + url);

								@SuppressLint("StaticFieldLeak") HttpRequestTask metadataTask = new HttpRequestTask(
										m_activity, ttircUrl + "/backend.php", m_api.getSessionId()) {
									@Override
									protected void onPostExecute(JsonElement result) {
										//Log.d(TAG, "got metadata for URL: " + url + " " + result);

										if (result != null && result.isJsonObject()) {
											m_urlMetadata.put(url, result.getAsJsonObject());
										} else {
											JsonObject placeholder = new JsonObject();
											placeholder.addProperty("no-metadata", true);

											m_urlMetadata.put(url, placeholder);
										}

										m_messageAdapter.notifyDataSetChanged();
									}
								};

								HashMap<String, String> params = new HashMap<String, String>() {
									{
										put("op", "urlmetadata");
										put("url", url);
									}
								};

								metadataTask.execute(params);
							}
						}
					}

					break;
				default:
				    span = new SpannableString(ts + " " + fmt.getString("message"));
				    
					span.setSpan(new ForegroundColorSpan(tsColor), 0, ts.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					
					span = rewriteEmoticons(span);

                    holder.message.setText(span);
				}
				
				switch (Integer.parseInt(m_prefs.getString("font_size", "0"))) {
				case 0:
                    holder.message.setTextSize(13F);
					break;
				case 1:
                    holder.message.setTextSize(16F);
					break;
				case 2:
                    holder.message.setTextSize(21F);
					break;		
				}

				CustomLinkify.onClickListener = new CustomLinkify.OnClickListener() {
					@Override
					public void onClick(String url) {
						if (m_activity != null) {
							try {
								url = proxyMediaURL(url, 0);

								m_activity.openUri(Uri.parse(url));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				};

				CustomLinkify.addLinks(holder.message, Linkify.ALL);
			}
		
			return v;
		}

		public int pxToDp(int px) {
			DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
			int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
			return dp;
		}

		public int dpToPx(int dp) {
			DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
			int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
			return px;
		}

		private void applyUrlMetadata(final String url, final MessageListHolder holder) {
			try {
				JsonObject metadata = m_urlMetadata.get(url);

				//Log.d(TAG, "applying metadata for url:" + url + " " + metadata);

				/* extended URL metadata */
				if (metadata.has("title")) {

					String title = metadata.get("title").getAsString();
					String imageUrl = metadata.has("image") ? metadata.get("image").getAsString() : null;
					String summary = metadata.has("descr") ? metadata.get("descr").getAsString() : "";

					holder.metadataThumbnail.setPadding(0, 0, 0, 0);
					holder.metadataContainer.setVisibility(View.VISIBLE);
					holder.metadataTitle.setText(title);
					holder.metadataSummary.setText(summary);
					holder.metadataContainer.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								m_activity.openUri(Uri.parse(url));
							}
						});

					TypedValue tv = new TypedValue();
					m_activity.getTheme().resolveAttribute(R.attr.ic_action_photo, tv, true);

					if (imageUrl != null) {
						imageUrl = proxyMediaURL(imageUrl, 300);

						Glide.with(m_activity)
								.load(imageUrl)
								.diskCacheStrategy(DiskCacheStrategy.ALL)
								.skipMemoryCache(false)
								.error(tv.resourceId)
								.listener(new RequestListener<String, GlideDrawable>() {
									@Override
									public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
										holder.metadataThumbnail.setPadding(0, 0, 0, 0);
										return false;
									}

									@Override
									public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
										int padding = dpToPx(8);

										holder.metadataThumbnail.setPadding(padding, padding, padding, padding);
										return false;
									}
								})
								.into(holder.metadataThumbnail);
					} else {
						holder.metadataThumbnail.setImageResource(tv.resourceId);
					}


				} else if (metadata.has("content-type")) {
					/* check if its an image instead  */

					String contentType = metadata.get("content-type").getAsString();

					if (contentType.contains("image/") || contentType.contains("video/")) {
						//Log.d(TAG, "metadata is image: " + url);

						holder.imageThumbnail.getLayoutParams().height = (int)(m_screenHeight * 0.25f);
						holder.imageThumbnail.setVisibility(View.VISIBLE);

						final String thumbnailUrl = proxyMediaURL(url, 300);

						holder.imageThumbnail.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								m_activity.openUri(Uri.parse(proxyMediaURL(url, 0)));
							}
						});

						Glide.with(m_activity)
								.load(thumbnailUrl)
								.diskCacheStrategy(DiskCacheStrategy.ALL)
								.skipMemoryCache(false)
								.listener(new RequestListener<String, GlideDrawable>() {
									@Override
									public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
										holder.imageThumbnail.setVisibility(View.GONE);
										return false;
									}

									@Override
									public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
										holder.imageThumbnail.setVisibility(View.VISIBLE);
										return false;
									}
								})
								.into(holder.imageThumbnail);

					}
				}

			} catch (Exception e) {
				Log.d(TAG, "error processing metadata for URL: " + url);

				e.printStackTrace();
			}
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}
		
		Bundle formatMessage(Message message) {
			Bundle rv = new Bundle();

			switch (message.message_type) {
			case Constants.MSGT_PRIVMSG:
			case Constants.MSGT_PRIVATE_PRIVMSG:
			case Constants.MSGT_NOTICE:
			case Constants.MSGT_TOPIC:
				rv.putString("sender", message.sender);
				rv.putString("message", message.message);
				break;		
			case Constants.MSGT_ACTION:
				rv.putString("message", " ✱ " + message.sender + " " + message.message);
				break;
			case Constants.MSGT_EVENT:
				rv.putString("sender", message.sender);
				rv.putString("message", " ❖ " + formatEvent(message));
				break;
			case Constants.MSGT_SYSTEM:
				rv.putString("message", " ❖ " + message.message);
			default:
				rv.putString("message", message.message);
				break;
			}		
			return rv;
		}
		
		String formatEvent(Message message) {
			
			try {
				String[] params = message.message.split(":", 3);
				MessageEventType eventType = MessageEventType.valueOf(params[0]);
				
				switch (eventType) {
				case NICK:
					return getString(R.string.event_nick, message.sender, params[1]);
				case TOPIC:
					return getString(R.string.event_topic, message.sender, message.message.replace("TOPIC:", ""));				
				case MODE:
					if (params[1].equals("")) {
						return getString(R.string.event_mode_user, message.channel, params[2]);
					} else {
						return getString(R.string.event_mode_chan, message.sender, params[1], params[2]);
					}				
				case KICK:
					return getString(R.string.event_kick, params[1], message.channel, message.sender, params[2]);				
				case PART:
					return getString(R.string.event_part, params[1], message.channel, params[2]);				
				case JOIN:
					return getString(R.string.event_join, params[1], params[2], message.channel);
				case QUIT:
					return getString(R.string.event_quit, message.sender, message.message.replace("QUIT:", ""));				
				case DISCONNECT:
					return getString(R.string.event_disconnect);				
				case REQUEST_CONNECTION:
					return getString(R.string.event_request_connection);
				case CONNECTING:
					return getString(R.string.event_connecting, params[1], params[2]);
				case PING_REPLY:
					return getString(R.string.event_ping_reply, message.sender, params[1]);
				case NOTICE:
					return "NOTICE/FIXME: " + message.message; // FIXME handle event NOTICE properly
				case CTCP:
					return getString(R.string.event_ctcp, params[1], params[2], message.sender);				
				case CTCP_REPLY:
					return getString(R.string.event_ctcp_reply, params[1], message.sender, params[2]);
				case PING:
					return getString(R.string.event_ping, params[1], message.sender);				
				case CONNECT:
					return getString(R.string.event_connect);
				case UNKNOWN_CMD:
					return getString(R.string.event_unknown_cmd, params[1]);
				case ERROR:
					return message.message.replace("ERROR:", "");
				case SERVER_PONG:
					return getString(R.string.event_server_pong, params[1]);
				case UNAWAY:
					return getString(R.string.event_unaway, message.sender);
				default:
					Log.d(TAG, "formatEvent error:" + message.message);
					return null;
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				Log.d(TAG, "formatEvent error:" + message.message);
				return message.message;
			}
		}
	}

	private String proxyMediaURL(String url, int resize) {

		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.US);
		String ymd = df.format(Calendar.getInstance().getTime());

		String cid = MainActivity.sha1(m_api.getSessionId() + ymd);

		//Log.d(TAG, "sid=" + m_api.getSessionId() + " " + ymd + " => " + cid);

		return m_prefs.getString("ttirc_url", "") +
				"/backend.php?op=imgproxy&url=" + URLEncoder.encode(url) +
				"&cid=" + cid + "&resize=" + resize;
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_NULL || 
				event.getAction() == KeyEvent.ACTION_DOWN || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

			messagesToBottom();

			String input = v.getText().toString();

			if (input.length() > 0) {
				m_activity.showProgressBar(true);

				m_api.send(m_connectionId, m_channelName, input, new UpdateServiceApi.OnSendCompleted() {
					@Override
					public void onSendCompleted(JsonElement result) {
						m_activity.showProgressBar(false);
					}
				});
				
				v.setText("");

			}
		}		
		return true;
	}

	private void messagesToBottom() {
		final ListView messageList = getView().findViewById(R.id.channel_message_list);

		if (messageList != null) {
			messageList.postDelayed(new Runnable() {
				@Override
				public void run() {
					messageList.setSelection(m_messageAdapter.getCount() - 1);
				}
			}, 100);
		}
	}

	private SpannableString rewriteEmoticons(SpannableString span) {
		if (m_prefs.getBoolean("enable_emoticons", true)) {
			Pattern pattern = Pattern.compile(":([^ :]+):");
			Matcher matcher = pattern.matcher(span);

			while (matcher.find()) {
				String key = matcher.group();

				if (!m_emoticonBitmaps.containsKey(key) && m_emoticons.containsKey(key)) {

					Emoticon emot = m_emoticons.get(key);
					Bitmap bitmap = m_activity.loadEmoticonBitmap(emot);

					if (bitmap != null) {
						m_emoticonBitmaps.put(key, bitmap);
					}
				}

				if (m_emoticonBitmaps.containsKey(key)) {
					ImageSpan imgSpan = new ImageSpan(m_activity, m_emoticonBitmaps.get(key), ImageSpan.ALIGN_BOTTOM);
					span.setSpan(imgSpan, matcher.start(), matcher.end(), SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
			}
		}
		
		return span;
	}

	@Override
	public int getMessagesCount() {
		return m_messages.size();
	}
}
