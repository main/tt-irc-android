package org.fox.ttirc;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.fox.ttirc.backend.UpdateService;
import org.fox.ttirc.net.HttpRequestTask;

import java.util.HashMap;

public class LoginActivity extends CommonActivity {
	private final String TAG = this.getClass().getSimpleName();

	private SharedPreferences m_prefs;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		m_prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

	}

	protected void setStatus(int id, boolean progress) {
		TextView tv = findViewById(R.id.login_status_text);
		if (tv != null) {
			tv.setText(id);
		}
		
		//setProgressBarIndeterminateVisibility(progress);
	}

	@Override
	public void onResume() {
		super.onResume();
		
		Intent serviceIntent = new Intent(LoginActivity.this, UpdateService.class);
		stopService(serviceIntent);

		if (isConfigured()) {
			if (getIntent().getBooleanExtra("autoLogin", true)) {				
				login();
			} else {
				setStatus(R.string.login_ready, false);
			}
		} else {
			setStatus(R.string.login_need_configure, false);
			
			//intent = new Intent(this, PreferencesActivity.class);
			//startActivityForResult(intent, 0);
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.dialog_need_configure_prompt)
			       .setCancelable(false)
			       .setPositiveButton(R.string.dialog_open_preferences, new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			   			// launch preferences
			   			
			        	   Intent intent = new Intent(LoginActivity.this,
			        			   PreferencesActivity.class);
			        	   startActivityForResult(intent, 0);
			           }
			       })
			       .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			                dialog.cancel();
			           }
			       });
			AlertDialog alert = builder.create();
			alert.show();

		}
	}

	public boolean isConfigured() {
		String login = m_prefs.getString("login", "");
		String password = m_prefs.getString("password", "");
		String ttircUrl = m_prefs.getString("ttirc_url", "");

		return !(login.equals("") || password.equals("") || ttircUrl.equals(""));    	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.preferences:
			Intent intent = new Intent(this, PreferencesActivity.class);
			startActivityForResult(intent, 0);
			return true;
		case R.id.login:
			login();
			return true;
		case R.id.quit:			
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@SuppressWarnings("unchecked")
	protected boolean login() {
		setStatus(R.string.login_in_progress, true);

		final String ttircUrl = m_prefs.getString("ttirc_url", "");
		final String login = m_prefs.getString("login", "");
		final String password = m_prefs.getString("password", "");

		@SuppressLint("StaticFieldLeak") HttpRequestTask task = new HttpRequestTask(getApplicationContext(),
				ttircUrl + "/backend.php", "") {
			@Override
			protected void onPostExecute(JsonElement result) {
				if (result != null) {
					JsonObject resultObj = result.getAsJsonObject();
					
					JsonElement sid = resultObj.get("sid");
					JsonElement version = resultObj.get("version");
					JsonElement csrfToken = resultObj.get("csrf_token");
					
					if (version == null) {
						m_lastError = ErrorType.INVALID_SERVER_VERSION;
						return;
					}
					
					if (sid == null) {
						m_lastError = ErrorType.NOT_AUTHORIZED;
						return;
					}
					
					String sessionId = sid.getAsString();

					// Start service
					
					Intent intent = new Intent(LoginActivity.this, UpdateService.class);
					intent.putExtra("sessionId", sessionId);
					intent.putExtra("csrfToken", csrfToken != null ? csrfToken.getAsString() : "");

					startService(intent);
					bindService(intent, m_serviceConnection, Context.BIND_AUTO_CREATE);

					// Switch to Main activity
					intent = new Intent(getApplicationContext(), MainActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					overridePendingTransition(0, 0);
					
					startActivityForResult(intent, 0);
					
					try {
						unbindService(m_serviceConnection);
					} catch (IllegalArgumentException e) {
						// service wasn't registered yet, nvm
					}

					//finish();
					
				} else {
					setStatus(getErrorMessage(m_lastError), false);
				}
			}
		};

		@SuppressWarnings("serial")
		HashMap<String, String> map = new HashMap<String, String>() {
			{
				put("op", "login");
				put("mobile", "1");
				put("user", login);
				put("password", password);
			}
		};
		
		task.execute(map);
    	
		return false;
	}

	ServiceConnection m_serviceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.i(TAG, "Service connection established");
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.i(TAG, "Service connection closed");
		}			
	};

	@Override
	public void onDestroy() {
		try {
			unbindService(m_serviceConnection);

		} catch (IllegalArgumentException e) {
			// service wasn't registered yet, nvm
		}
		super.onDestroy();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		Intent serviceIntent = new Intent(LoginActivity.this, UpdateService.class);
		stopService(serviceIntent);
		finish();
	}
}