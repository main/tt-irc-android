package org.fox.ttirc.utils;

import android.content.Intent;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import androidx.core.app.JobIntentService;

public class GetFileService extends JobIntentService {
	private final String TAG = this.getClass().getSimpleName();
	
	private String m_httpLogin;
	private String m_httpPassword;

	@Override
	protected void onHandleWork(Intent intent) {
		String fetchUrl = intent.getStringExtra("fetchUrl");
		String outputFile = intent.getStringExtra("outputFile");
		
		if (fetchUrl != null && outputFile != null) {
			m_httpLogin = intent.getStringExtra("httpLogin");
			m_httpPassword = intent.getStringExtra("httpPassword");
		
			Log.d(TAG, "Downloading: " + fetchUrl + " => " + outputFile);
		
			downloadFile(fetchUrl, outputFile);
		}
		
	}

	private void downloadFile(String fetchUrl, String outputFile) {
		try {
			URL url = new URL(fetchUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			if (m_httpLogin.length() > 0) {
				conn.setRequestProperty("Authorization", "Basic " + 
					Base64.encodeToString((m_httpLogin + ":" + m_httpPassword).getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP));
			}

			InputStream content = conn.getInputStream();

			BufferedInputStream is = new BufferedInputStream(content, 1024);
			FileOutputStream fos = new FileOutputStream(outputFile);
			
			byte[] buffer = new byte[1024];
			int len;
			while ((len = is.read(buffer)) != -1) {
			    fos.write(buffer, 0, len);
			}
			
			fos.close();
			is.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
