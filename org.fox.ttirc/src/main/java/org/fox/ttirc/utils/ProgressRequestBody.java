package org.fox.ttirc.utils;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

public class ProgressRequestBody extends RequestBody {

    private static final int SEGMENT_SIZE = 2048; // okio.Segment.SIZE

    private final InputStream m_stream;
    private final ProgressListener m_listener;
    private final String m_contentType;

    public ProgressRequestBody(InputStream stream, String contentType, ProgressListener listener) {
        m_stream = stream;
        m_listener = listener;
        m_contentType = contentType;
    }

    @Override
    public long contentLength() throws IOException {
        return m_stream.available();
    }

    @Override
    public MediaType contentType() {
        return MediaType.parse(m_contentType);
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        Source source = null;
        try {
            source = Okio.source(m_stream);
            long total = 0;
            long read;

            while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                total += read;
                sink.flush();
                this.m_listener.transferred(total);

            }
        } finally {
            Util.closeQuietly(source);
        }
    }

    public interface ProgressListener {
        void transferred(long num);
    }

}