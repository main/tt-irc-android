package org.fox.ttirc;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.livefront.bridge.Bridge;

import org.fox.ttirc.types.Constants;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.browser.customtabs.CustomTabsCallback;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.browser.customtabs.CustomTabsSession;
import icepick.State;

public class CommonActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
	private final String TAG = this.getClass().getSimpleName();
	
	public static final String THEME_DEFAULT = "THEME_FOLLOW_DEVICE";

	public final static String NOTIFICATION_CHANNEL_GROUP = "channel_group_chat";
	public final static String NOTIFICATION_CHANNEL_PRIORITY = "channel_priority";
	public final static String NOTIFICATION_CHANNEL_SERVICE = "channel_service";

	public final static String NOTIFICATION_CHANNEL_GROUP_NAME = "Group chats";
	public final static String NOTIFICATION_CHANNEL_PRIORITY_NAME = "Highlights and private chats";
	public final static String NOTIFICATION_CHANNEL_SERVICE_NAME = "Service notifications";

	public static final String INTENT_ACTION_EXIT = "org.fox.ttirc.INTENT_ACTION_EXIT";
	public static final String INTENT_ACTION_LOGOUT = "org.fox.ttirc.INTENT_ACTION_LOGOUT";

	public final static int REQUEST_IMAGE_PICKER = 101;

	private boolean m_smallScreenMode = true;
	@State String m_themeName;
	protected SharedPreferences m_prefs;
	private boolean m_needRestart;

	private static String m_customTabPackageName;

	static final String STABLE_PACKAGE = "com.android.chrome";
	static final String BETA_PACKAGE = "com.chrome.beta";
	static final String DEV_PACKAGE = "com.chrome.dev";
	static final String LOCAL_PACKAGE = "com.google.android.apps.chrome";

	private static final String ACTION_CUSTOM_TABS_CONNECTION =
			"android.support.customtabs.action.CustomTabsService";

	protected CustomTabsClient m_customTabClient;
	protected CustomTabsServiceConnection m_customTabServiceConnection = new CustomTabsServiceConnection() {
		@Override
		public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
			m_customTabClient = customTabsClient;

			m_customTabClient.warmup(0);
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			m_customTabClient = null;
		}
	};

	protected boolean isUiNightMode() {
		try {
			int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;

			return Configuration.UI_MODE_NIGHT_YES == nightModeFlags;
		} catch (Exception e) {
			return false;
		}
	}

	protected void setAppTheme(SharedPreferences prefs) {
		String theme = prefs.getString("theme", CommonActivity.THEME_DEFAULT);

		Log.d(TAG, "setting theme to: " + theme);

		if ("THEME_DARK".equals(theme) || "THEME_HOLO".equals(theme)) {
			AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
		} else if ("THEME_LIGHT".equals(theme)) {
			AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
		} else {
			AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
		}

		setTheme(R.style.AppTheme);
	}

	protected void setSmallScreen(boolean smallScreen) {
		Log.d(TAG, "m_smallScreenMode=" + smallScreen);
		m_smallScreenMode = smallScreen;
	}
	
	public void toast(int msgId) {
		Toast toast = Toast.makeText(CommonActivity.this, msgId, Toast.LENGTH_SHORT);
		toast.show();
	}

	public void toast(String msg) {
		Toast toast = Toast.makeText(CommonActivity.this, msg, Toast.LENGTH_SHORT);
		toast.show();
	}

	@Override
	public void onDestroy() {

		if (m_customTabServiceConnection != null) {
			unbindService(m_customTabServiceConnection);
		}

		super.onDestroy();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationManager nmgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

			// todo: human readable names

			NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_PRIORITY,
					NOTIFICATION_CHANNEL_PRIORITY_NAME,
					NotificationManager.IMPORTANCE_HIGH);
			channel.setShowBadge(false);
			channel.enableLights(true);
			channel.enableVibration(true);
			//channel.setSound(null, null);
			nmgr.createNotificationChannel(channel);

			channel = new NotificationChannel(NOTIFICATION_CHANNEL_GROUP,
					NOTIFICATION_CHANNEL_GROUP_NAME,
					NotificationManager.IMPORTANCE_DEFAULT);
			channel.setShowBadge(false);
			channel.enableLights(true);
			channel.setSound(null, null);
			nmgr.createNotificationChannel(channel);

			channel = new NotificationChannel(NOTIFICATION_CHANNEL_SERVICE,
					NOTIFICATION_CHANNEL_SERVICE_NAME,
					NotificationManager.IMPORTANCE_MIN);
			channel.setShowBadge(false);
			channel.enableLights(false);
			channel.setSound(null, null);
			nmgr.createNotificationChannel(channel);
		}

		m_prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		m_prefs.registerOnSharedPreferenceChangeListener(this);

		setAppTheme(m_prefs);

		Bridge.restoreInstanceState(this, savedInstanceState);

		if (savedInstanceState == null) {
			m_themeName = m_prefs.getString("theme", THEME_DEFAULT);
		}

		String customTabPackageName = getCustomTabPackageName(this);

		CustomTabsClient.bindCustomTabsService(this, customTabPackageName != null ?
				customTabPackageName : "com.android.chrome", m_customTabServiceConnection);

		super.onCreate(savedInstanceState);
	}

	private static String getCustomTabPackageName(Context context) {
		if (m_customTabPackageName != null) return m_customTabPackageName;

		PackageManager pm = context.getPackageManager();
		// Get default VIEW intent handler.
		Intent activityIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.example.com"));
		ResolveInfo defaultViewHandlerInfo = pm.resolveActivity(activityIntent, 0);
		String defaultViewHandlerPackageName = null;
		if (defaultViewHandlerInfo != null) {
			defaultViewHandlerPackageName = defaultViewHandlerInfo.activityInfo.packageName;
		}

		// Get all apps that can handle VIEW intents.
		List<ResolveInfo> resolvedActivityList = pm.queryIntentActivities(activityIntent, 0);
		List<String> packagesSupportingCustomTabs = new ArrayList<>();
		for (ResolveInfo info : resolvedActivityList) {
			Intent serviceIntent = new Intent();
			serviceIntent.setAction(ACTION_CUSTOM_TABS_CONNECTION);
			serviceIntent.setPackage(info.activityInfo.packageName);
			if (pm.resolveService(serviceIntent, 0) != null) {
				packagesSupportingCustomTabs.add(info.activityInfo.packageName);
			}
		}

		// Now packagesSupportingCustomTabs contains all apps that can handle both VIEW intents
		// and service calls.
		if (packagesSupportingCustomTabs.isEmpty()) {
			m_customTabPackageName = null;
		} else if (packagesSupportingCustomTabs.size() == 1) {
			m_customTabPackageName = packagesSupportingCustomTabs.get(0);
		} else if (!TextUtils.isEmpty(defaultViewHandlerPackageName)
				&& packagesSupportingCustomTabs.contains(defaultViewHandlerPackageName)) {
			m_customTabPackageName = defaultViewHandlerPackageName;
		} else if (packagesSupportingCustomTabs.contains(STABLE_PACKAGE)) {
			m_customTabPackageName = STABLE_PACKAGE;
		} else if (packagesSupportingCustomTabs.contains(BETA_PACKAGE)) {
			m_customTabPackageName = BETA_PACKAGE;
		} else if (packagesSupportingCustomTabs.contains(DEV_PACKAGE)) {
			m_customTabPackageName = DEV_PACKAGE;
		} else if (packagesSupportingCustomTabs.contains(LOCAL_PACKAGE)) {
			m_customTabPackageName = LOCAL_PACKAGE;
		}

		return m_customTabPackageName;
	}

	public boolean isSmallScreen() {
		return m_smallScreenMode;
	}

	@SuppressWarnings("deprecation")
	public boolean isPortrait() {
		Display display = getWindowManager().getDefaultDisplay(); 
		
	    int width = display.getWidth();
	    int height = display.getHeight();
		
	    return width < height;
	}

	@Override
	public void onResume() {
		super.onResume();

		if (m_needRestart) {
			Log.d(TAG, "shared preferences changed, restarting");

			finish();
			startActivity(getIntent());
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		Log.d(TAG, "onSharedPreferenceChanged:" + key);

		if ("theme".equals(key)) {
			setAppTheme(sharedPreferences);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle out) {
		super.onSaveInstanceState(out);
		
		Bridge.saveInstanceState(this, out);
	}

	private CustomTabsSession getCustomTabSession() {
		return m_customTabClient.newSession(new CustomTabsCallback() {
			@Override
			public void onNavigationEvent(int navigationEvent, Bundle extras) {
				super.onNavigationEvent(navigationEvent, extras);
			}
		});
	}

	private void openUriWithCustomTab(Uri uri) {
		if (m_customTabClient != null) {
			TypedValue tvBackground = new TypedValue();
			getTheme().resolveAttribute(R.attr.colorPrimary, tvBackground, true);

			CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder(getCustomTabSession());

			builder.setStartAnimations(this, R.anim.slide_in_right, R.anim.slide_out_left);
			builder.setExitAnimations(this, R.anim.slide_in_left, R.anim.slide_out_right);

			builder.setToolbarColor(tvBackground.data);

			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType("text/plain");
			shareIntent.putExtra(Intent.EXTRA_TEXT, uri.toString());

			PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
					Constants.NOTIFY_CHROME_SHARE, shareIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			builder.setActionButton(BitmapFactory.decodeResource(getResources(), R.drawable.ic_share),
					getString(R.string.share_url), pendingIntent);

			CustomTabsIntent intent = builder.build();

			intent.launchUrl(this, uri);
		}
	}

	// uses chrome custom tabs when available
	public void openUri(final Uri uri) {
		boolean enableCustomTabs = m_prefs.getBoolean("enable_custom_tabs", true);
		final boolean askEveryTime = m_prefs.getBoolean("custom_tabs_ask_always", true);

		if (enableCustomTabs && m_customTabClient != null) {

			if (askEveryTime) {

				View dialogView = View.inflate(this, R.layout.dialog_open_link_askcb, null);
				final CheckBox askEveryTimeCB = dialogView.findViewById(R.id.open_link_ask_checkbox);

				AlertDialog.Builder builder = new AlertDialog.Builder(
						CommonActivity.this)
						.setTitle(R.string.open_link)
						.setView(dialogView)
						.setMessage(uri.toString())
						.setPositiveButton(R.string.quick_preview,
								new Dialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
														int which) {

										if (!askEveryTimeCB.isChecked()) {
											SharedPreferences.Editor editor = m_prefs.edit();
											editor.putBoolean("custom_tabs_ask_always", false);
											editor.apply();
										}

										openUriWithCustomTab(uri);

									}
								})
						.setNegativeButton(R.string.open_with_app,
								new Dialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
														int which) {

										if (!askEveryTimeCB.isChecked()) {
											SharedPreferences.Editor editor = m_prefs.edit();
											editor.putBoolean("custom_tabs_ask_always", false);
											editor.putBoolean("enable_custom_tabs", false);
											editor.apply();
										}

										Intent intent = new Intent(Intent.ACTION_VIEW, uri);

										startActivity(intent);

									}
								});
						/*.setNegativeButton(R.string.cancel,
							new Dialog.OnClickListener() {
								public void onClick(DialogInterface dialog,
													int which) {

									if (!askEveryTimeCB.isChecked()) {
										SharedPreferences.Editor editor = m_prefs.edit();
										editor.putBoolean("custom_tabs_ask_always", false);
										editor.apply();
									}

								}
							});*/

				AlertDialog dlg = builder.create();
				dlg.show();

			} else {
				openUriWithCustomTab(uri);
			}

		} else {
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);

			startActivity(intent);
		}
	}


	@SuppressWarnings("deprecation")
	public void copyToClipboard(String str) {
		if (android.os.Build.VERSION.SDK_INT < 11) {				
			android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			clipboard.setText(str);
		} else {
			android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			clipboard.setText(str);
		}		

		toast(R.string.text_copied_to_clipboard);
	}
}
