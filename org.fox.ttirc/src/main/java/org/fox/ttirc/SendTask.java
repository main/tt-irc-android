package org.fox.ttirc;

import android.content.Context;

import org.fox.ttirc.net.HttpRequestTask;
import org.fox.ttirc.types.Constants;

import java.util.HashMap;

public class SendTask extends HttpRequestTask {
	private final String TAG = this.getClass().getSimpleName();

	private String m_channelName;
	private int m_connectionId;
	private String m_csrfToken;
	
	protected SendTask(Context context, int connectionId, String channelName, String sessionId, String csrfToken) {
		super(context, null, sessionId);

		m_requestUrl = m_prefs.getString("ttirc_url", "").trim() + "/backend.php?op=send";

		m_connectionId = connectionId;
		m_channelName = channelName;
		m_csrfToken = csrfToken;
	}
	
	public void execute(final String message) {
		final String tabType;

		if (m_channelName.equals("---")) {
			tabType = Constants.TAB_SERVER;
		} else if (m_channelName.indexOf('#') != 0) {
			tabType = Constants.TAB_PRIVMSG;
		} else {
			tabType = Constants.TAB_CHANNEL;
		}

		@SuppressWarnings("serial")
		HashMap<String, String> map = new HashMap<String, String>() {
			{
				put("csrf_token", m_csrfToken);
				put("chan", m_channelName);
				put("message", message);
				put("connection", String.valueOf(m_connectionId));
				put("tab_type", tabType);
			}
		};

		super.execute(map);
	}

}
