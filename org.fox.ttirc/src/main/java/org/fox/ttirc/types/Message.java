package org.fox.ttirc.types;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

public class Message implements Parcelable {
	public int id;
	public int message_type;
	public String sender;
	public String channel;
	public int connection_id;
	public boolean incoming;
	public String message;
	public String ts;
	public int sender_color;
	public Date date; // not deserialized from JSON
	
	public Message() {
		//
	}
	
	public Message(Parcel in) {
		readFromParcel(in);
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(id);
		out.writeInt(message_type);
		out.writeString(sender);
		out.writeString(channel);
		out.writeInt(connection_id);
		out.writeInt(incoming ? 1 : 0);
		out.writeString(message);
		out.writeString(ts);
		out.writeInt(sender_color);
		out.writeLong(date.getTime());
	}
	
	public void readFromParcel(Parcel in) {
		id = in.readInt();
		message_type = in.readInt();
		sender = in.readString();
		channel = in.readString();
		connection_id = in.readInt();
		incoming = in.readInt() == 1;
		message = in.readString();
		ts = in.readString();
		sender_color = in.readInt();
		date = new Date(in.readLong());
		
	}
	
	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR =
    	new Parcelable.Creator() {
            public Message createFromParcel(Parcel in) {
                return new Message(in);
            }
 
            public Message[] newArray(int size) {
                return new Message[size];
            }
        };
}