package org.fox.ttirc.types;

import java.io.Serializable;

import ir.mirrajabi.searchdialog.core.Searchable;

public class Emoticon implements Serializable, Searchable {
    public String file;
    public String localfile;
    public int height;
    public String hint;
    public String name;

    @Override
    public String getTitle() {
        return name;
    }
}
