package org.fox.ttirc.types;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Channel implements Parcelable, Serializable {
	public int chan_type;
	public List<String> users;
	public List<String> topic;
	
	public Channel() {
		//
	}
	
	public Channel(Parcel in) {
		readFromParcel(in);
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(chan_type);
		out.writeStringList(users);
		out.writeStringList(topic);
	}
	
	public void readFromParcel(Parcel in) {
		chan_type = in.readInt();
		if (users == null) users = new ArrayList<String>();
		in.readStringList(users);
		if (topic == null) topic = new ArrayList<String>();
		in.readStringList(topic);
	}
	
	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR =
    	new Parcelable.Creator() {
            public Channel createFromParcel(Parcel in) {
                return new Channel(in);
            }
 
            public Channel[] newArray(int size) {
                return new Channel[size];
            }
        };
	
}
