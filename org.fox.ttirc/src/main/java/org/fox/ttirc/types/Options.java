package org.fox.ttirc.types;

import java.util.List;

public class Options {
	public boolean hide_join_part;
	public List<String> highlight_on;
	public String uniqid;
	public boolean fetch_url_titles;
	public boolean disable_image_preview;
	public List<String> emoticons_favorite;
	public String sid;
	public int default_buffer_size;
}
