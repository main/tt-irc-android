package org.fox.ttirc.types;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Connection implements Parcelable, Serializable {
	public int id;
	public String active_server;
	public String active_nick;
	public int status;
	public String title;
	public Map<String, List<String>> userhosts;
	
	@SuppressWarnings("serial")
	public class UserHosts extends HashMap<String, List<String>> {
		//
	}
	
	public Connection() {
		//
	}
	
	public Connection(Parcel in) {
		readFromParcel(in);
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(id);
		out.writeString(active_server);
		out.writeString(active_nick);
		out.writeInt(status);
		out.writeString(title);
		out.writeMap(userhosts);
	}
	
	public void readFromParcel(Parcel in) {
		id = in.readInt();
		active_server = in.readString();
		active_nick = in.readString();
		status = in.readInt();
		title = in.readString();
		if (userhosts == null) userhosts = new UserHosts();
		in.readMap(userhosts, UserHosts.class.getClassLoader());
	}
	
	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR =
    	new Parcelable.Creator() {
            public Connection createFromParcel(Parcel in) {
                return new Connection(in);
            }
 
            public Connection[] newArray(int size) {
                return new Connection[size];
            }
        };
}